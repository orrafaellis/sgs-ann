    clear;close all;
    well = load('well.mat');
    well = well.well;
    columns = well.curve_info(:,1);

    properties = {'NPHI','VS'};
    depth = well.curves(3500:8000,find(ismember(columns,'DEPTH')));
    output = well.curves(3500:8000,find(ismember(columns,properties)));
    
%normaliza��o:    
    output0 = output;
    par1 = output - min(output);
    par2 = par1./max(par1);
    output = par2;
    
%defini��o do grid:
    out_simulation = -999*ones(size(depth,1),1+size(output,2));
    out_simulation(:,1)= depth;
    num_cells = length(depth);

%plot e defini��o de ponto condicionante:
    figure('units','normalized','outerposition',[0 0 1 1]);
    subplot(2, 3, 1);
    indexes = ~sum(isnan(output)&output==-999,2);
    for plot_id = 1:size(output,2)
        plot(depth(indexes),output(indexes,plot_id));
        hold on;
    end
    
    cp = 1; 
    
    
    cp_log = zeros(size(depth));
    
    cond_method = 3;
    for a = 1:1
        switch cond_method
            case 1
            %selecionar pontos condicionantes:
                if cp
                   for a=1:1
                        pause(0.001);
                        rect_info = getrect();
                        interval = 1;
                        selected_interval = [rect_info(1) rect_info(1)+rect_info(3)];
                        start_cp = find(abs(depth-selected_interval(1))==min(abs(depth-selected_interval(1))));
                        end_cp = find(abs(depth-selected_interval(2))==min(abs(depth-selected_interval(2))));

                        start_cp=49;
                        end_cp=452;


                        cp_log(start_cp:end_cp) = 1;
                    end
                end

            case 2
            %gerar intervalo de condicionais
                cp_log((output~=-999)&(~isnan(output))) = 1;
            case 3
            %gerar condicionais rand�micos:
                pause(0.001);
                rect_info = getrect();
                interval = 1;
                selected_interval = [rect_info(1) rect_info(1)+rect_info(3)];
                start_cp = find(abs(depth-selected_interval(1))==min(abs(depth-selected_interval(1))));
                end_cp = find(abs(depth-selected_interval(2))==min(abs(depth-selected_interval(2))));
                start_cp=28;
                end_cp=2230;
                indices = (start_cp:end_cp)';
    %             indices = indices(~isnan(output)&output~=-999);
%                   indices = indices(randi(length(indices),1,450));
                cp_log(indices) = 1;
        end
    end
    
    cp_log = logical(cp_log);
%     [output(cp_log) params_nscore] = nscore(output(cp_log));
    out_simulation(cp_log,2:end) = output(cp_log,:);
    plot(depth(cp_log),cp_log(cp_log),'*','color','red');
    legend([properties,'pontos condicionantes'])
 
    
    subplot(2, 3, 3);
    plot(output(cp_log,1),output(cp_log,2),'x');
    title(['Crossplot:  ',properties{1},' and ',properties{2}]);
    xlabel(properties{1});ylabel(properties{2});
 %treinamento da rede
    training_data = [depth,output];
    training_data = training_data(cp_log,:);
    
    
       %plotar variogramas e histograma dos dados
    for plot_id = 1:size(output,2)
        x = output(cp_log,plot_id);
        x= x - mean(x);
        covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
        covariogram = covariogram / max(covariogram);
        subplot(6, 6, 2+plot_id);
        plot(covariogram);
        title(['Variogram for ',properties{plot_id}, ', R: ',num2str(get_cpnoise_range(training_data(:,1+plot_id),1))]);
        subplot(6, 6, 8+plot_id);
        histogram(output(cp_log,plot_id));
        title(['Histogram for ',properties{plot_id}]);
    end
    
    
    
    %configura��o
    range = max(training_data) - min(training_data);
%     delta = 0.45; tau = 0.1; vmin = 2; spmin = 3; covtype = 'full';
    options = [];
    options.Display = 'final';
    %tau controlou o n�vel de ruido:inversamente proporcional
    model = igmn('CovType', 'full', 'range', range, 'tau',0.1, 'delta', 0.05, 'vmin', 40, 'spmin',...
        3, 'Options', options,'corr_coef',0,'range_trend',1500);
%     model = igmn('range',range,'delta',delta,'tau',tau,'vmin',vmin,'spmin',spmin,'covtype', covtype);
    model = model.train(training_data);
%     subplot(2, 3, 1);
%     %plotar as gaussianas
%     for cluster_id = 1:model.nc
%         mean_i = model.means(cluster_id,:);
%         cov_i = model.covs(:,:,cluster_id);
%         plot(mean_i(1),mean_i(2),'v','color',[201, 34, 227]/255);
%         plot_gaussian_ellipsoid(mean_i, cov_i);
%     end
%     
%ru�do:
% range_noise = get_cpnoise_range(training_data(:,2),2);
range_noise = 35;
noise = fftma_l3c(length(out_simulation),2,range_noise,range_noise,0)-0.5*ones(length(out_simulation),size(output,2));
noise = noise./std(noise);

%simula��o
    available_points_index = logical(ones(length(depth),1));
    %figure;
%     for sim_id = 1:sum(double(~cp_log))
    means =[];
    num_points_sim = length(depth);
    
    for sim_id = 1:num_points_sim
        
        waitbar(sim_id/num_points_sim);
        available_points = (1:length(depth))';
        available_points = available_points(available_points_index);
        actual_p_index = randi(length(available_points));
        actual_p = available_points(actual_p_index);
        available_points_index(actual_p) = false;
        
        %estimar media e cov
        dist_params = model.recall3(depth(actual_p));
        medias = dist_params{1};
        cov_matrix = dist_params{2};
        log_point = mvnrnd(medias,dist_params{2},1);
         for plot_id = 1:size(output,2)
%             std_i = sqrt(cov_matrix(plot_id,plot_id));
%             log_point_i = medias(plot_id) + noise(actual_p,plot_id)*std_i;
%             out_simulation(actual_p,1+plot_id) = log_point_i;
%             
            subplot(4, 3, 4+3*plot_id);
            plot(depth(actual_p),medias(plot_id),'x','color','red');hold on; 
            plot(depth(actual_p),log_point(plot_id),'x','color','black');hold on; 
            title(['Log:  ',properties{plot_id}]);
         end
        subplot(2, 3, 6);
        plot(log_point(1),log_point(2),'x');hold on;  
        title(['Experimental crossplot:  ',properties{1},' and ',properties{2}]);
        out_simulation(actual_p,2:end) = log_point;
        

        %adicionar ponto � rede:
        model = model.train(out_simulation(actual_p,:));

    end
    
    subplot(2, 3, 6);cla
    plot(out_simulation(:,2),out_simulation(:,3),'x');
    
    for plot_id = 1:size(output,2)
        x = out_simulation(:,plot_id+1);
        x= x - mean(x);
        covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
        covariogram = covariogram / max(covariogram);
        subplot(6, 6, 20+plot_id);
        plot(covariogram);
        title(['Variogram for ',properties{plot_id}, ', R: ',num2str(get_cpnoise_range(out_simulation(:,1+plot_id),1))]);
        subplot(6, 6, 26+plot_id);
        histogram(out_simulation(:,plot_id+1));
        title(['Histogram for ',properties{plot_id}]);
    end
    
    subplot(2, 3, 4);cla
    for plot_id = 1:size(output,2)
        plot(depth(indexes),out_simulation(indexes,1+plot_id));
        hold on;
    end
    legend(properties)
    
%     
%     
%     
% %     out_simulation(:,2) = inscore(out_simulation(:,2),params_nscore);
%     means(:,2) = inscore(means(:,2),params_nscore);
%     
%     
%     
%     subplot(2, 3, 4);cla
%     plot(means(:,1),means(:,2),'x','color','red'); 
%     plot(out_simulation(out_simulation(:,2)~=-999,1),out_simulation(out_simulation(:,2)~=-999,2),'color','black');hold on;
% % plot(depth(cp_log),output(cp_log),'x','color','green');
% hold on
% %plotar as m�dias das gaussianas
% for cluster_id = 1:model.nc
%    mean_i = model.means(cluster_id,:);
%    plot(mean_i(1),mean_i(2),'o','color',[179, 30, 201]/255);
% end
% 
% %variograma da simula��o
% x = out_simulation(:,2);
% x= x - mean(x);
% covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
% covariogram = covariogram / max(covariogram);
% subplot(2, 3, 5);cla;
% plot(covariogram);hold on
% [~,idx]=sort(means(:,1));
% 
% x = means(idx,2);
% x= x - mean(x);
% covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
% covariogram = covariogram / max(covariogram);
% subplot(2, 3, 5);
% plot(covariogram);hold off
% 
% subplot(2, 3, 6);cla
% histogram(out_simulation(:,2));
