    clear;close all;
    well = load('well.mat');
    well = well.well;
    columns = well.curve_info(:,1);

%defini��o do grid
    depth = well.curves(:,find(ismember(columns,'DEPTH')));
    output = well.curves(:,find(ismember(columns,'VP')));
    out_simulation = -999*ones(size(depth,1),2);
    out_simulation(:,1)= depth;
    num_cells = length(depth);

%plot e defini��o de ponto condicionante:
    figure;
    plot(depth(output~=-999),output(output~=-999),'color','green');
    hold on;
    cp = 1; 
    
    
    cp_log = zeros(size(depth));
    if cp
       for a=1:3
            pause(0.001);
            rect_info = getrect();
            interval = 1;
            selected_interval = [rect_info(1) rect_info(1)+rect_info(3)];
            start_cp = find(abs(depth-selected_interval(1))==min(abs(depth-selected_interval(1))));
            end_cp = find(abs(depth-selected_interval(2))==min(abs(depth-selected_interval(2))));
            cp_log(start_cp:end_cp) = 1;
        end
    end
       
    cp_log = logical(cp_log);
    out_simulation(cp_log,2) = output(cp_log);
%     plot(depth(cp_log),output(cp_log),'o','color','red');
    
    
 %treinamento da rede
    training_data = [depth,output];
    training_data = training_data(cp_log,:);
    %configura��o
    range = 2*(max(training_data) - min(training_data));
%     delta = 0.45; tau = 0.1; vmin = 2; spmin = 3; covtype = 'full';
    options = [];
    options.Display = 'final';
    
    model = old_igmn('CovType', 'full', 'range', range, 'tau',0.1, 'delta', 0.1, 'vmin', 1000, 'spmin', 3, 'Options', options,'corr_coef',0.4);
%     model = igmn('range',range,'delta',delta,'tau',tau,'vmin',vmin,'spmin',spmin,'covtype', covtype);
    model = model.train(training_data);
 
%simula��o
    available_points_index = ~cp_log;
    %figure;
%     for sim_id = 1:sum(double(~cp_log))
    for sim_id = 1:400
%         waitbar(sim_id/sum(double(~cp_log)));
        available_points = (1:length(depth))';
        available_points = available_points(available_points_index);
        actual_p_index = randi(length(available_points));
        actual_p = available_points(actual_p_index);
        available_points_index(actual_p) = false;
        
        %estimar ponto:
        log_point = model.recall(depth(actual_p));
        out_simulation(actual_p,2) = log_point;
        
        %adicionar ponto � rede:
        model.train(out_simulation(actual_p,:));
        pause(0.001);
        hold on;
    end
    figure;
    plot(out_simulation(out_simulation(:,2)~=-999,1),out_simulation(out_simulation(:,2)~=-999,2),'x','color','blue');hold on;
%     plot(depth(cp_log),output(cp_log),'x','color','red');
hold on
%plotar as m�dias das gaussianas
for cluster_id = 1:model.nc
   mean_i = model.means(cluster_id,:);
   plot(mean_i(1),mean_i(2),'o','color',[179, 30, 201]/255);
end