    clear;%close all;
    well = load('well.mat');
    well = well.well;
    columns = well.curve_info(:,1);

%defini��o do grid
    t=3000;
    depth = well.curves(t:t+1000,find(ismember(columns,'DEPTH')));
    output = well.curves(t:t+1000,find(ismember(columns,'VS')));
    out_simulation = -999*ones(size(depth,1),2);
    out_simulation(:,1)= depth;
    num_cells = length(depth);

%plot e defini��o de ponto condicionante:
    figure;
    subplot(2, 2, 1);
    plot(depth(output~=-999),output(output~=-999),'color','black');
    hold on;
    cp = 1; 
    
    
    cp_log = zeros(size(depth));
    
    cond_method = 2;
    switch cond_method
        case 1
        %selecionar pontos condicionantes:
            if cp
               for a=1:1
                    pause(0.001);
                    rect_info = getrect();
                    interval = 1;
                    selected_interval = [rect_info(1) rect_info(1)+rect_info(3)];
                    start_cp = find(abs(depth-selected_interval(1))==min(abs(depth-selected_interval(1))));
                    end_cp = find(abs(depth-selected_interval(2))==min(abs(depth-selected_interval(2))));
                    cp_log(start_cp:end_cp) = 1;
                end
            end

        case 2
        %gerar intervalo de condicionais
            cp_log(1:length(depth)) = 1;
        case 3
        %gerar condicionais rand�micos:
%             pause(0.001);
%             rect_info = getrect();
%             interval = 1;
%             selected_interval = [rect_info(1) rect_info(1)+rect_info(3)];
%             start_cp = find(abs(depth-selected_interval(1))==min(abs(depth-selected_interval(1))));
%             end_cp = find(abs(depth-selected_interval(2))==min(abs(depth-selected_interval(2))));
            start_cp=2000;
            end_cp=8000;
            indices = (start_cp:end_cp)';
%             indices = indices(~isnan(output)&output~=-999);
            indices = indices(randi(length(indices),1,1000));
            cp_log(indices) = 1;
    end
    
    cp_log = logical(cp_log);
    out_simulation(cp_log,2) = output(cp_log);
    colormap([1 1 1;1 0 0]);
%     imagesc([min(depth(output~=-999)),max(depth(output~=-999))], depth(output~=-999), (cp_log(output~=-999)+1)');hold on;
    plot(depth(cp_log),output(cp_log),'o','color','red');
    plot(depth(output~=-999),output(output~=-999),'color','black');
    
    %plotar variograma dos dados
    
%          x = output((output~=-999)&~isnan(output));
          x = output(cp_log);
        x= x - mean(x);
        covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
        covariogram = covariogram / max(covariogram);
        subplot(2, 2, 2);
        plot(covariogram);
    %
    
    
 %treinamento da rede
    training_data = [depth,output];
    training_data = training_data(cp_log,:);
    %configura��o
    range = max(training_data) - min(training_data);
%     delta = 0.45; tau = 0.1; vmin = 2; spmin = 3; covtype = 'full';
    options = [];
    options.Display = 'final';
    %tau controlou o n�vel de ruido:inversamente proporcional
    model = igmn('CovType', 'full', 'range', range, 'tau',0.05, 'delta', 0.06, 'vmin', 1*e5, 'spmin', 4, 'Options', options,'corr_coef',0,'range_trend',35);
%     model = igmn('range',range,'delta',delta,'tau',tau,'vmin',vmin,'spmin',spmin,'covtype', covtype);
    model = model.train(training_data);
 
%simula��o
    available_points_index = logical(ones(length(depth),1));
    %figure;
%     for sim_id = 1:sum(double(~cp_log))
    means =[];
    num_points_sim = length(depth);
    
    for sim_id = 1:num_points_sim
        
        waitbar(sim_id/num_points_sim);
        available_points = (1:length(depth))';
        available_points = available_points(available_points_index);
        actual_p_index = randi(length(available_points));
        actual_p = available_points(actual_p_index);
        available_points_index(actual_p) = false;
        
        %estimar media e cov
        dist_params = model.recall(depth(actual_p));
        means = [means;depth(actual_p),dist_params(1)];
        %catar ponto:
        if sum(isnan(dist_params))>0
           eita = 1;  
        end
        log_point = mvnrnd(dist_params(1),dist_params(2),1);
        out_simulation(actual_p,2) = log_point;
        
        %adicionar ponto � rede:
        model.train(out_simulation(~available_points_index,:));
        pause(0.001);
        hold on;
    end
    
    
    
    
    
    
    
    subplot(2, 2, 3);
    plot(out_simulation(out_simulation(:,2)~=-999,1),out_simulation(out_simulation(:,2)~=-999,2),'color','blue');hold on;
% plot(means(:,1),means(:,2),'x','color','red'); 
% plot(depth(cp_log),output(cp_log),'x','color','green');
hold on
%plotar as m�dias das gaussianas
for cluster_id = 1:model.nc
   mean_i = model.means(cluster_id,:);
   plot(mean_i(1),mean_i(2),'o','color',[179, 30, 201]/255);
end

%variograma da simula��o
x = out_simulation(:,2);
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram = covariogram / max(covariogram);
subplot(2, 2, 4);
plot(covariogram);