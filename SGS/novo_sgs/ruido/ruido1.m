close all;clear


varianc = 100;
meann = 40;
n_points = 100000;



rand_v = fftma_l3c(n_points,1,500,150,1);





figure;
% subplot(2,2,3);
% plot(rand_v);
out = gen_values_pdf(varianc,meann,n_points,rand_v);

subplot(2,2,1);histogram(out);

x = out;
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram = covariogram / max(covariogram);
subplot(2,2,2);
plot(covariogram);

function out = gen_values_pdf(varianc,meann,n_points,rand_v)
    r0=randn(n_points,1);
    r0 = sqrt(varianc)*r0+meann;
    r1 = sort(r0);
    cum_x = linspace(0,1,n_points);
    figure;plot(cum_x,n_points*r1/max(r1));
%     position = interp1(cum_x,n_points*r1/max(r1),rand_v,'nearest');
    position = interp1(cum_x,r1,rand_v,'nearest');
    
%     position(position<1) = 1;
    out = position;
%     out = r1(round(position));    
end

% 
% 
% function out = gen_values_pdf(varianc,meann,n_points,rand_v)
%     r0=randn(n_points,1);
%     r0 = sqrt(varianc)*r0+meann;
%     r1 = sort(r0);
%     cum_x = linspace(0,1,n_points);
%     figure;plot(cum_x,n_points*r1/max(r1));
%     position = interp1(cum_x,n_points*r1/max(r1),rand_v,'nearest');
%     position(position<1) = 1;
%     out = r1(round(position));    
% end