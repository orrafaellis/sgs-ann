    clear;close all;
    well = load('well.mat');
    well = well.well;
    columns = well.curve_info(:,1);

%defini��o do grid
    
    depth = well.curves(7000:7300,find(ismember(columns,'DEPTH')));
    output = well.curves(7000:7300,find(ismember(columns,'VS')));
%     output0 = output;
%     par1 = output - min(output);
%     par2 = par1./max(par1);
%     output = par2;
    out_simulation = -999*ones(size(depth,1),2);
    out_simulation(:,1)= depth;
    num_cells = length(depth);

%plot e defini��o de ponto condicionante:
    figure('units','normalized','outerposition',[0 0 1 1]);
    subplot(2,3,1);
     plot(depth(output~=-999),output(output~=-999),'x','color','black');hold on;
    cp = 1; 
    
    
    cp_log = zeros(size(depth));
      indices = 1:length(depth);
%       indices = indices(randi(length(indices),1,50));
      indices = indices(100:150);
%      indices = [96    91   140   248   136   289   135    21    58   168   286   186   245   170   162     3    87   198   263   215 221   227    19   235     6   106   202   176   239    25]; 
    
     cp_log(indices) = 1;
    cp_log = logical(cp_log);
    out_simulation(cp_log,2) = output(cp_log);
    

    plot(depth(cp_log),output(cp_log),'o','color','red');hold on;
%     plot(depth(output~=-999),output(output~=-999),'color','black');
    
    %plotar variograma dos dados
    
%          x = output((output~=-999)&~isnan(output));
          x = output(cp_log);
        x= x - mean(x);
        covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
        covariogram = covariogram / max(covariogram);
        subplot(2, 3, 2);
        plot(covariogram);
   subplot(2, 3, 3);
        histogram(output(cp_log));
    
 %treinamento da rede
    training_data = [depth,output];
    training_data = training_data(cp_log,:);
    %configura��o
    range = max(training_data) - min(training_data);
%     delta = 0.45; tau = 0.1; vmin = 2; spmin = 3; covtype = 'full';
    options = [];
    options.Display = 'final';
    %tau controlou o n�vel de ruido:inversamente proporcional
    model = igmn('CovType', 'full', 'range', range, 'tau',0.08, 'delta', 0.03, 'vmin',...
        60, 'spmin', 3, 'Options', options,'corr_coef',0,'range_trend',5);
%     model = igmn('range',range,'delta',delta,'tau',tau,'vmin',vmin,'spmin',spmin,'covtype', covtype);
    model = model.train(training_data);
 
% % % %     plotar gaussianas:::
subplot(2, 3, 1);
    for cluster_id = 1:model.nc
        mean_i = model.means(cluster_id,:);subplot(2, 3, 1);
        cov_i = model.covs(:,:,cluster_id);
        plot(mean_i(1),mean_i(2),'v','color',[201, 34, 227]/255);
        plot_gaussian_ellipsoid(mean_i, cov_i);
    end
    

%simula��o
    available_points_index = true(length(depth),1);
    %figure;
%     for sim_id = 1:sum(double(~cp_log))
    simulated =[];
    means = [];
    num_points_sim = length(depth);
    
    for sim_id = 1:num_points_sim
        
        waitbar(sim_id/num_points_sim);
        available_points = (1:length(depth))';
        available_points = available_points(available_points_index);
        actual_p_index = randi(length(available_points));
        actual_p = available_points(actual_p_index);
        available_points_index(actual_p) = false;
        
        %estimar media e cov
        dist_params = model.recall(depth(actual_p));
        means = [means;depth(actual_p),dist_params(1)];
        %catar ponto:
        
        log_point = mvnrnd(dist_params(1),dist_params(2),1);
        out_simulation(actual_p,2) = log_point;
       
        dist_params(2)
%         plot(depth(actual_p),log_point,'x','color','black');
%         plot(depth(actual_p),dist_params(1),'+','color','red');

        %adicionar ponto � rede:
        model = model.train(out_simulation(actual_p,:));
        simulated = [simulated;depth(actual_p),log_point];
        subplot(2, 3, 4);cla;
        for cluster_id = 1:model.nc
                mean_i = model.means(cluster_id,:);
                cov_i = model.covs(:,:,cluster_id);
                plot(mean_i(1),mean_i(2),'v','color',[201, 34, 227]/255);
                plot_gaussian_ellipsoid(mean_i, cov_i);
        end
          plot(means(:,1),means(:,2),'*','color','blue');
        plot(simulated(:,1),simulated(:,2),'x','color','black');
%         plot(depth(cp_log),output(cp_log),'o','color','red');hold on;
        hold on;
    end
%     
%     
%     
%     
%     
   
%     out_simulation(:,2) = out_simulation(:,2)*max(par1)+min(output0);
%     means(:,2) = means(:,2)*max(par1)+min(output0);
    
    subplot(2, 3, 4);
    plot(out_simulation(out_simulation(:,2)~=-999,1),out_simulation(out_simulation(:,2)~=-999,2),'color','blue');
    hold on;
    [~,order]= sort(means(:,1));
    means = means(order,:);
    plot(means(:,1),means(:,2),'x','color','red'); 
 plot(depth(cp_log),output(cp_log),'x','color','green');
% hold on
% %plotar as m�dias das gaussianas
% for cluster_id = 1:model.nc
%    mean_i = model.means(cluster_id,:);
%    plot(mean_i(1),mean_i(2),'o','color',[179, 30, 201]/255);
% end
% 
%variograma da simula��o
x = out_simulation(:,2);
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram = covariogram / max(covariogram);
subplot(2, 3, 5);
plot(covariogram);
subplot(2, 3, 6);
histogram(out_simulation(:,2));
