function [ noises ] = fftma_l3c(m,n, range_h, range_v,err)
%FFTMA_L3C Summary of this function goes here
%   Detailed explanation goes here
%FFTMA
range_vertical = range_v;
range_horizontal = range_h;
noises = randn(m, n);
[correlation_function] = construct_correlation_function_beta(range_vertical,range_horizontal,noises,1);
noises = FFT_MA_3D(correlation_function,noises);
%FFTMA
if err 
    noises = erf(noises * 0.7);
end
% noises = reshape(noises, m , n);
noises = noises - repmat(min(noises),size(noises,1),1);
noises = noises ./ repmat(max(noises),size(noises,1),1);
end

