function range = get_cpnoise_range(cp_vec,m)
    noise = cp_vec - mean(cp_vec);
    
    x = noise;
    x= x - mean(x);
    covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
    covariogram = covariogram / max(covariogram);    
    switch m
        case 1
            first_neg_position = find(covariogram<0,1);
            last_positive_position = first_neg_position-1;
            range = interp1(covariogram([last_positive_position,first_neg_position]),[last_positive_position,first_neg_position],0,'linear');
        case 2
    %achar primeiro m�nimo local
%             figure;plot(covariogram);hold on;
            windowSize = 100; 
            b = (1/windowSize)*ones(1,windowSize);
            filtrado = filter(b,1,covariogram);
            range = find(islocalmin(filtrado),1);
    end
    

end

