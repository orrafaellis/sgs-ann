classdef siigmn
    
    properties
        name = 'SIIGMN';
        
        % configuration params
        SIGMA = [];
        delta;
        tau;
        maxDist;
        spmin;
        vmin;
        uniform = false;
        covType;
        regVal;
        options;
        covAngle; %inserted by sergio
       
        % components params
        priors = [];
        means = [];
        covs = [];
        sps = [];
        vs = [];
        nc = 0;
  
        % Mahalanobis distance
        mahalaD;
        
        % sample size
        sampleSize = 0;
        
        % model likelihood
        dataLikelihood = 0;
        
        % information criteria 
        BIC;
        AIC;
        
        % components outputs
        loglike = [];
        post = [];
    end
    
    methods
        
        %Constructor
        function obj = siigmn(varargin)
           
            pnames = {'range' 'sigma' 'delta' 'tau' 'vmin' 'spmin' 'uniform' 'covtype' 'regularize' 'covangle' 'options'};
            dflts =  {[] [] 0.01 0.1 3 2 false 'full' 0 0 []};
            [range sigma delta tau vmin spmin uniform covType regV covAngle options] ...
                = internal.stats.parseArgs(pnames, dflts, varargin{:});
             
            options = statset(statset('gmdistribution'), options);
            
            if ~isnumeric(delta) || ~isscalar(delta) || delta < 0 || delta > 1
                error('Invalid delta.');
            end
            obj.delta = delta;
            
            if ~isempty(range) 
                if ~isrow(range)
                    error('Invalid range.');
                end
                dimension = size(range,2);
                obj.vmin = 2 * dimension;
                obj.spmin = dimension + 1;
                obj.SIGMA = (obj.delta * range).^2;
            else
                error('Range cannot be empty.');
            end
            
            if ~isempty(sigma) 
                if ~isrow(sigma)
                    error('Invalid sigma.');
                end
                obj.SIGMA = sigma;
            end
            
            if isempty(obj.SIGMA)
                warning('You must set the param SIGMA manually.');
            end
            
            if ~isnumeric(tau) || ~isscalar(tau) || tau < 0 || tau > 1
                error('Invalid tau.');
            end
            obj.tau = tau;
            obj.maxDist = chi2inv(1 - obj.tau, dimension);
            
            if ~isnumeric(vmin) || ~isscalar(vmin) || vmin < 0 
                error('Invalid vmin.');
            end
            obj.vmin = vmin;
            
            if ~isnumeric(spmin) || ~isscalar(spmin) || spmin < 0 
                error('Invalid spmin.');
            end
            obj.spmin = spmin;
            
            if ~islogical(uniform)
                error('Invalid uniform param.');
            end
            obj.uniform = uniform;
            
            if ischar(covType)
                covNames = {'diagonal', 'full'};
                i = find(strncmpi(covType, covNames, length(covType)));
                if isempty(i)
                    error('Invalid covariance type.');
                end
                CovType = i;
            else
                error('Invalid covariance type.');
            end
            obj.covType = CovType;

            if ~isnumeric(regV) || ~isscalar(regV) || regV < 0
                error('Invalid regularize value.');
            end
            obj.regVal = regV;
            
            %inserted by Sergio
            if ~isnumeric(covAngle)
                error('Invalid initial gaussian angle!');
            end
            obj.covAngle = covAngle;
                                    
            options.Display = find(strncmpi(options.Display, {'off', 'final', 'iter'}, length(options.Display))) - 1;
            obj.options = options;
        end
        
        % Compute the log probability density of a multivariate normal distribution
        function [log_lh, mahalaD, logDetCov] = logmvnpdf(obj, X, means, covs)
            [n,d] = size(X);
            k = size(means,1);
            log_lh = zeros(n, k);
            mahalaD = zeros(n, k);
            logDetCov = zeros(1, k);
            
            for j = 1:k
                if obj.covType == 2 % full covariance
                    [L,f] = chol(covs(:,:,j));
                    diagL = diag(L);
                    if ((f ~= 0) || any(abs(diagL) < eps(max(abs(diagL))) * size(L,1)))
                        error('Ill Conditioned Covariance.');
                    end
                    logDetCov(j) = 2 * sum(log(diagL));
                 else % diagonal covariance
                    L = sqrt(covs(:,:,j)); % a vector 
                    if  any(L < eps(max(L)) * d)
                        error('Ill Conditioned Covariance.');
                    end
                    logDetCov(j) = sum(log(covs(:,:,j)));
                end
                Xcentered = bsxfun(@minus, X, means(j,:));
    
                if obj.covType == 2
                    xRinv = Xcentered / L;
                else
                    xRinv = bsxfun(@times, Xcentered , (1 ./ L));
                end
                
                mahalaD(:,j) = sum(xRinv.^2, 2);
                log_lh(:,j) = - 0.5 * (mahalaD(:,j) + logDetCov(j) + d * log(2 * pi));              
            end
        end
        
        % Compute the log probability density of the multivariate normal distribution
        % evaluated for each IGMN component
        function obj = computeLikelihood(obj, x)
            if obj.nc > 0
                [obj.loglike, obj.mahalaD] = obj.logmvnpdf(x, obj.means, obj.covs);
            end
        end
        
        % Compute the posterior probability for each IGMN component
        function obj = computePosterior(obj)
            logPrior = log(obj.priors);
            obj.post = bsxfun(@plus, obj.loglike, logPrior);
            maxll = max(obj.post, [], 2);
            % minus maxll to avoid underflow
            obj.post = exp(bsxfun(@minus, obj.post, maxll));
            density = sum(obj.post, 2);
            % normalize
            obj.post = bsxfun(@rdivide, obj.post, density);
            logpdf = log(density) + maxll;
            obj.dataLikelihood = obj.dataLikelihood + sum(logpdf);
        end
        
        % Check the IGMN novelty criterion
        function h = hasAcceptableDistribution(obj)
            for j = 1:obj.nc
            	if (obj.mahalaD(j) <= obj.maxDist)
                    h = true;
                    return;
                end
            end
            
            h = false;
        end
        
        % Create a component when a data point x matches the novelty criterion
        function obj = createComponent(obj, x)
            obj.nc = obj.nc + 1;
            obj.means(obj.nc,:) = x;
            obj.sps(1,obj.nc) = 1.0;
            obj.vs(1,obj.nc) = 0;
            obj = obj.updatePriors();
            if obj.covType == 2                
                 %obj.covs(:,:,obj.nc) = diag(obj.SIGMA);
                 
                 %prints matrix
%                  diag(obj.SIGMA)
%                  obj.covs(1:2,1:2,obj.nc)
%                  pause;
                 
                 angle_rad = (obj.covAngle/180)*pi;
                 
%                  fprintf('\n Angle: %f   Angle_rad: %f \n',obj.covAngle,angle_rad);
%                  pause;
                 
                 if(size(obj.means,2) == 3)
                     R = [cos(angle_rad) -sin(angle_rad) 0; ...
                          sin(angle_rad)  cos(angle_rad) 0; ...
                               0           0     1];

                     obj.covs(:,:,obj.nc) = R * diag(obj.SIGMA) * R'; %rotates cov matrix
                 else
                     obj.covs(:,:,obj.nc) = diag(obj.SIGMA);
                 end
                     
                         
                 
                 %prints matrix
%                  obj.covs(1:2,1:2,obj.nc)
%                  pause;
                 
                 %obj.covs(1,2,obj.nc) = 30000;
                 %obj.covs(2,1,obj.nc) = 30000;
            else
                 obj.covs(:,:,obj.nc) = obj.SIGMA;
            end
            [obj.loglike(1,obj.nc), obj.mahalaD(1, obj.nc)] = obj.logmvnpdf(x, obj.means(obj.nc,:), obj.covs(:,:,obj.nc));
        end
        
        % Update the IGMN priors
        function obj = updatePriors(obj)
            if ~obj.uniform
                obj.priors = obj.sps ./ sum(obj.sps);
             else
                obj.priors = ones(1,obj.nc) ./ obj.nc;
            end
        end
        
        % Update the IGMN parameters 
        function obj = updateComponents(obj, x)
            obj.sps = obj.sps + obj.post;
            obj.vs = obj.vs + 1;
            
            w = obj.post ./ obj.sps; 
           
            d = size(obj.means,2);
            for j = 1:obj.nc
                Xcentered = x - obj.means(j,:);
                deltaMU = w(j) * Xcentered;
                obj.means(j,:) = obj.means(j,:) + deltaMU; 
                
                XcenteredNEW = x - obj.means(j,:);    

                if obj.covType == 2 % full covariance
                    obj.covs(:,:,j) = obj.covs(:,:,j) - deltaMU' * deltaMU + w(j) * (XcenteredNEW' * XcenteredNEW - obj.covs(:,:,j))...
                        + obj.regVal * eye(d);
                else % diagonal covariance 
                    obj.covs(:,:,j) = obj.covs(:,:,j) - deltaMU.^2 + w(j) * (XcenteredNEW.^2 - obj.covs(:,:,j))...
                        + obj.regVal;
                end
            end
            
            % normalize the priors
            obj = obj.updatePriors();
        end 
        
        % Remove spurious components
        function obj = removeSpurious(obj)
            for i = obj.nc:-1:1
                if (obj.sps(i) < obj.spmin && obj.vs(i) > obj.vmin)
                    obj.nc = obj.nc - 1;
                    obj.priors(i) = [];
                    obj.means(i,:) = [];
                    obj.covs(:,:,i) = []; 
                    obj.vs(i) = [];
                    obj.sps(i) = [];                    
                    obj.post(i) = [];
                    obj.mahalaD(i) = [];
                    obj.loglike(i) = [];
                end
            end
        end
        
        % Train the IGMN with the data set X
        function obj = train(obj, X)
            dispfmt = '%6d\t%12g';
            
            if obj.options.Display > 1 % 'iter'
                fprintf('  iter\t    log-likelihood\n');
            end
            
            N = size(X,1);
            
            for i = 1:N
                
          %      if (mod(i,100) == 0) fprintf('Training input: %d / %d - number of components: %d\n', i, N, obj.nc); end
     
                x = X(i,:);
                
                obj = obj.computeLikelihood(x);
                if (~obj.hasAcceptableDistribution())
                    obj = obj.createComponent(x);
                end
                obj = obj.computePosterior();
                
                obj = obj.updateComponents(x);
                obj = obj.removeSpurious();
                
                if obj.options.Display > 1 %'iter'
                    disp(sprintf(dispfmt, i, obj.dataLikelihood));
                end
    
            end
             
            if obj.options.Display > 0 % 'final' or 'iter'
                fprintf('%d iterations, log-likelihood = %g\n', i, obj.dataLikelihood);
            end
            
            obj.sampleSize = obj.sampleSize + N;
            
            obj = obj.computeInfoCriteria();
        end
        
       % IGMN recalling algorithm
        function B = recall_old(obj, X)
            if isempty(X)
                error('Input cannnot be empty.');
            end;
            if obj.nc > 0 && size(X,2) == size(obj.means, 2)
                error('Input vector must has the dimension of the vector <a> where x = [a,b] was the sample used for training and <b> is the label vector.');
            end;
            
            N = size(X, 1);
            B = zeros(N, size(obj.means,2) - size(X, 2));
            for j = 1:N
                x = X(j,:);
                
                alpha = size(x, 2);
                beta = size(obj.means, 2) - alpha;

                pajs = zeros(obj.nc, 1);
                xm = zeros(obj.nc, beta);

                for i=1:obj.nc
                    meanA = obj.means(i, 1:alpha);
                    meanB = obj.means(i, alpha+1:alpha+beta);

                    if obj.covType == 2 % full covariance
                        covA = obj.covs(1:alpha, 1:alpha, i);
                        covBA = obj.covs(alpha+1:alpha+beta, 1:alpha, i);
                       
                        xm(i,:) = meanB + (covBA / covA * (x - meanA)')';
                    else
                        covA = obj.covs(1:alpha); %TODO: conferir se isso não está errado
                       
                        xm(i,:) = meanB;
                    end
                    loglike = obj.logmvnpdf(x, meanA, covA);
                    pajs(i) = exp(loglike) * obj.priors(i);
                end

                pajs = pajs ./ sum(pajs);
                B(j,:) = sum(bsxfun(@times, xm, pajs));
            end
        end
        
         % IGMN recalling algorithm with Trend and Variance calculation
        function B = recall(obj, X, b_trend)
            if isempty(X)
                error('Input cannnot be empty.');
            end;
            if obj.nc > 0 && size(X,2) >= size(obj.means, 2)
                error('Input vector must has the dimension of the vector <a> where x = [a,b] was the sample used for training and <b> is the label vector.');
            end;
            if isempty(b_trend)
                b_trend = mean(obj.means(:,size(X,2)+1:end));
            end;
            if size(b_trend,2) ~= (size(obj.means,2)-size(X,2))
                error('Trend vector incomplete!');
            end;                               
            
            N = size(X, 1);
            B = zeros(N, size(obj.means,2) - size(X, 2));
            C = zeros(size(obj.means,2) - size(X, 2), size(obj.means,2) - size(X, 2),N);

            for j = 1:N
                x = X(j,:);
                
                alpha = size(x, 2);
                beta = size(obj.means, 2) - alpha;

                pajs = zeros(obj.nc+1, 1);
                xm = zeros(obj.nc+1, beta);
                covm = zeros(size(obj.means,2) - size(X, 2), size(obj.means,2) - size(X, 2), obj.nc+1); %verificar isso direitinho
                
                %inserted by sergio
                smallestDist = 999999; %will store smallest MahalaD from all components
                smallestI = 1; %will store component's index for smallestDist

                for i=1:obj.nc
                    meanA = obj.means(i, 1:alpha);
                    meanB = obj.means(i, alpha+1:alpha+beta);

                    if obj.covType == 2 % full covariance
                        covA = obj.covs(1:alpha, 1:alpha, i);
                        covBA = obj.covs(alpha+1:alpha+beta, 1:alpha, i);
                        covBA_div_covA = covBA / covA;
                        xm(i,:) = meanB + (covBA_div_covA * (x - meanA)')';
                        
                        covBB = obj.covs(alpha+1:alpha+beta,alpha+1:alpha+beta,i);
                        covm(:,:,i) = covBB-covBA_div_covA*covBA'; %covBB - covBA*inv(covA)*covBA' -> used to calculate (co)variance of estimates
                    else
                        covA = obj.covs(1:alpha); %acho que isso está errado. Suspeito que deveria ser: covA = obj.covs(1:alpha,i);
                       
                        covm(:,:,i) = obj.covs(alpha+1:alpha+beta);
                        
                        xm(i,:) = meanB;
                    end
                    [loglike,dist,~] = obj.logmvnpdf(x, meanA, covA);
                    pajs(i) = exp(loglike) * obj.priors(i);
                    
                    %Finds closest gaussian
                    if dist < smallestDist
                        smallestDist=dist;
                        smallestI = i;
                    end;
                end
                
                %builds the moving trend gaussian component based on distance of closest gaussian
                    a_trend = x + (obj.means(smallestI,1:alpha) - x) * (6/smallestDist); 
                    %a_trend = x;
                    trend_mean = [a_trend b_trend]; %trend_mean = [u_x u_y trend(1) trend(2) ...]
                    trend_cov = obj.covs(:,:,smallestI); %copy closest gaussian cov matrix
                    if obj.covType == 2 % full covariance
                        %covA = obj.covs(1:alpha, 1:alpha, i);
                        covA = trend_cov(1:alpha, 1:alpha);
                        %covBA = obj.covs(alpha+1:alpha+beta, 1:alpha, i);
                        covBA = trend_cov(alpha+1:alpha+beta,1:alpha);

                        %xm(i,:) = meanB + (covBA / covA * (x - meanA)')';
                        xm(obj.nc+1,:) = b_trend + (covBA / covA * (x - a_trend)')';
                    else
                        %covA = obj.covs(1:alpha); 
                        covA = trend_cov(1:alpha);
                        xm(obj.nc+1,:) = b_trend;
                    end
                    loglike = obj.logmvnpdf(x, a_trend, covA);
                    pajs(obj.nc+1) = exp(loglike) * obj.priors(smallestI);
                %END - built moving gaussian trend
                    
                pajs = pajs ./ sum(pajs);
                B(j,:) = sum(bsxfun(@times, xm, pajs)); 
            end
        end  
        
        % IGMN recalling algorithm with Trend and Variance calculation
        function [B C] = recall_v(obj, X, b_trend)
            if isempty(X)
                error('Input cannnot be empty.');
            end;
            if obj.nc > 0 && size(X,2) >= size(obj.means, 2)
                error('Input vector must has the dimension of the vector <a> where x = [a,b] was the sample used for training and <b> is the label vector.');
            end;
            if nargin < 3
                b_trend = mean(obj.means(:,size(X,2)+1:end));
            end;
%             if size(b_trend,2) ~= (size(obj.means,2)-size(X,2))
%                 size(b_trend)
%                 size(obj.means)
%                 error('Trend vector incomplete!');
%                 pause;
%                 %TODO: complete with default trend
%             end;                               
            
            N = size(X, 1);
            B = zeros(N, size(obj.means,2) - size(X, 2));
            C = zeros(size(obj.means,2) - size(X, 2), size(obj.means,2) - size(X, 2),N);

            %calculates global variance
            
            GLOBAL_VARIANCE=0;
            index=size(obj.means,2);
            for j=1:obj.nc
                if obj.covs(index,index,j) > GLOBAL_VARIANCE
                    GLOBAL_VARIANCE = obj.covs(index,index,j);
                end
            end
            GLOBAL_VARIANCE = GLOBAL_VARIANCE*2;
            fprintf('\nGLOBAL_VARIANCE PARAMETER=%f\n',GLOBAL_VARIANCE);
            
            for j = 1:N
                x = X(j,:);
                
                alpha = size(x, 2);
                beta = size(obj.means, 2) - alpha;

                pajs = zeros(obj.nc+1, 1);
                xm = zeros(obj.nc+1, beta);
                covm = zeros(size(obj.means,2) - size(X, 2), size(obj.means,2) - size(X, 2), obj.nc+1); %verificar isso direitinho
                
                %inserted by sergio
                smallestDist = 9999999999999; %will store smallest MahalaD from all components
                smallestI = 1; %will store component's index for smallestDist
                dist = zeros(obj.nc);
                
                %to calculate moving gaussian component mean
                sum_mean_near = zeros(1,alpha);
                sum_count_near = 0;
                
                for i=1:obj.nc
                    meanA = obj.means(i, 1:alpha);
                    meanB = obj.means(i, alpha+1:alpha+beta);

                    if obj.covType == 2 % full covariance
                        covA = obj.covs(1:alpha, 1:alpha, i);
                        covBA = obj.covs(alpha+1:alpha+beta, 1:alpha, i);
                        covBA_div_covA = covBA / covA;
                        xm(i,:) = meanB + (covBA_div_covA * (x - meanA)')';
                        
                        covBB = obj.covs(alpha+1:alpha+beta,alpha+1:alpha+beta,i);
                        covm(:,:,i) = covBB-covBA_div_covA*covBA'; %covBB - covBA*inv(covA)*covBA' -> used to calculate (co)variance of estimates
                    else
                        covA = obj.covs(1:alpha); %acho que isso está errado. Suspeito que deveria ser: covA = obj.covs(1:alpha,i);
                       
                        covm(:,:,i) = obj.covs(alpha+1:alpha+beta);
                        
                        xm(i,:) = meanB;
                    end
                    [loglike,dist(i),~] = obj.logmvnpdf(x, meanA, covA);
                    pajs(i) = exp(loglike) * obj.priors(i);
                    
                    %Finds closest gaussian
                    if dist(i) < smallestDist
                        smallestDist=dist(i);
                        smallestI = i;
                    end
                    
                    
                    
                    RADIUS = 300;
                    %calculate the mean of all components in the radius
                    if(dist(i) < RADIUS)
                        sum_mean_near = sum_mean_near + obj.means(i,alpha+1:alpha+beta);
                        sum_count_near = sum_count_near+1;
                    end
                    
                    
                end
                    
                    if(sum_mean_near == 0)
                        b_trend = mean(obj.means(:,size(X,2)+1:end));
                        %error('sem medias!');
                    else
                        b_trend = sum_mean_near/sum_count_near;
                        
                    end
                    %builds the trend gaussian component based on distance of closest gaussian
                    a_trend = x + (obj.means(smallestI,1:alpha) - x) * (0.005/smallestDist); 
                    %a_trend = x;
                    trend_mean = [a_trend b_trend]; %trend_mean = [u_x u_y trend(1) trend(2) ...]
                    %trend_cov = obj.covs(:,:,smallestI); %copy closest gaussian cov matrix
                    trend_cov = diag(obj.SIGMA);
                    if obj.covType == 2 % full covariance
                        %covA = obj.covs(1:alpha, 1:alpha, i);
                        covA = trend_cov(1:alpha, 1:alpha);
                        %covBA = obj.covs(alpha+1:alpha+beta, 1:alpha, i);
                        covBA = trend_cov(alpha+1:alpha+beta,1:alpha);
                        
                        %xm(i,:) = meanB + (covBA / covA * (x - meanA)')';
                        covBA_div_covA = covBA / covA;
                        xm(obj.nc+1,:) = b_trend + (covBA_div_covA * (x - a_trend)')';                        
                        
                        covBB = trend_cov(alpha+1:alpha+beta,alpha+1:alpha+beta);
                        covm(:,:,obj.nc+1) = covBB-covBA_div_covA*covBA'; %covBB - covBA*inv(covA)*covBA' -> used to calculate (co)variance of estimates
                    else
                        %covA = obj.covs(1:alpha); 
                        covA = trend_cov(1:alpha);
                        xm(obj.nc+1,:) = b_trend;
                    end
                    loglike = obj.logmvnpdf(x, a_trend, covA);
                    pajs(obj.nc+1) = exp(loglike) * obj.priors(smallestI);                                    
                    
                pajs = pajs ./ sum(pajs);
                B(j,:) = sum(bsxfun(@times, xm, pajs)); 
                
                %calculation of variance
                %fprintf('meank\txmk\tBj\tpajs\tcovm\n');
                variogram_range=1;
                for k=1:obj.nc                    
                    contribution_to_variance_k = pajs(k)*(covm(:,:,k)+ sqrt(sum((xm(k,:)-B(j,:)).^2))^2);
                    
                    %Test 14/03/2016. Goal: reduce high frequency by making
                    %the variance proportional to the distance to known
                    %points:
                    
                    factor = (1-exp(-dist(k)/(variogram_range*10))); %exp
                    %factor = (3*dist(k))/(2*variogram_range) - (dist(k)^3)/(2*variogram_range^3);             
                    
                    contribution_to_variance_k = contribution_to_variance_k * factor;
                    
                    %contribution_to_variance_k = 0; 
                   
                    %Original variance                    
                    C(:,:,j) = C(:,:,j) + contribution_to_variance_k;
                end
                
                %TO CONTROL THE SILL OF THE VARIOGRAM
                if(smallestDist > variogram_range)
                    C(:,:,j) = GLOBAL_VARIANCE;
                end
                
%                 fprintf('\nValor estimado: %f\tvar: %f',B(j,:),C(:,:,j));
%                 fprintf('\nobj.means: ');
%                 disp(obj.means);
%                 fprintf('\nPajs: ');
%                 disp(pajs);
%                 pause;
                
%                 fprintf('\nC(:,:,j) = %.1f\n', C(:,:,j));
            end
        end  
       
        %IGMN - Sequential simulation algorithm in a trained model
        %'grid' are the locations (1 location per line) that will be simulated in a random path
        %'realizations' is an integer representing the number of times the grid will be simulated
        %'S' is a matrix [size(grid,1)][realizations] with resulting simulated data
        function S = simulate(obj,grid,realizations)
            grid_size = size(grid,1);
            
            S = zeros(grid_size,realizations);
            for i=1:realizations
                fprintf('IGMN - realization %d...\n',i);
                model = obj;
                
                randomPath = randsample(grid_size,grid_size);
                
                for j=1:grid_size
                    x = grid(randomPath(j),:);
                    
                    [igmn_est,igmn_var] = model.recall_v(x,[]); %estimates mean and variance from igmn
                    %igmn_est
                    igmn_var = (igmn_var + igmn_var')/2;
                    
                    sim = mvnrnd(igmn_est,igmn_var,1); %simulated cell taken from a normal distribution with mean=igmn_est and SIGMA=igmn_var

                    model = model.train([x sim]); %sequentially adds recent simulated cell as a training point
                    
%                     fprintf('\ngrid(%d): ',randomPath(j));
%                     disp(x);
%                     fprintf('\tSimulated value: ');
%                     disp(sim);
%                     pause;
                    
                    S(randomPath(j),i) = sim;
                end
            end            
        end
                
        % Calculate the indexes assignments for the data points in X for cluster analysis
        function idx = cluster(obj, X)
            N = size(X,1);
            idx = zeros(N,1);
            for i=1:N
                x = X(i,:);
                obj = obj.computeLikelihood(x);
                obj = obj.computePosterior();
                [~,idx(i)] = max(obj.post);
            end
        end
        
        % Compute AIC and BIC Information Criterion
        function obj = computeInfoCriteria(obj)
            dim = size(obj.means,2);
            if obj.covType == 1
                nParam = dim * obj.nc;
            else
                nParam = obj.nc * dim * (dim + 1) / 2;
            end
            NlogL = - obj.dataLikelihood;
            nParam = nParam + obj.nc - 1 + obj.nc * dim;
            obj.BIC = 2 * NlogL + nParam * log(obj.sampleSize);
            obj.AIC = 2 * NlogL + 2 * nParam;
        end
        
    end %end methods
    
    %builds a grid with size [nx*ny 2] starting at x0,y0 until (x0+dx*nx-1,y0+dy*ny-1)
    methods(Static)
        function grid = build_grid(nx,ny,dx,dy,x0,y0)
            grid = zeros(nx*ny,2);            
            for i=1:nx
                for j=1:ny
                    grid((i-1)*ny + j,:) = [(x0+dx*(i-1)) (y0+dy*(j-1))];
                end
            end
        end
    end
    
end % end class

