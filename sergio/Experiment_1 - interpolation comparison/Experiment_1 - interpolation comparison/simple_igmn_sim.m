x = [1  2  3  4  5  6  7  8  9  10  11  12  13  14  15  16  17  18  19  20]';
y = x;
z = [10 20 30 40 50 60 70 80 90 100 110 120 130 140 150 160 170 180 190 200]';
sim_x = [1.5 10.5 15.5]';
sim_y = sim_x;
realizations=50;
variogram = '1 Sph(3)';
%sim = sgsim([x y],z,[sim_x sim_y],variogram,options);

options = [];
options.Display = 'off';
%trend = mean(trainingSet.z);
data = [x y z];
delta = 0.1;
range = max(data)-min(data);
igmn_model = siigmn('range', range,'CovType', 'full', 'tau', 0.1, 'delta', delta, 'vmin', 0, 'spmin', 0, 'covAngle',0, 'Options', options);
igmn_model = igmn_model.train(data);%trains the model
igmn_sim = igmn_model.simulate([sim_x sim_y],realizations);
igmn_sim_mean = mean(igmn_sim,2)
igmn_sim_var = var(igmn_sim')'
[est_igmn var_igmn] = igmn_model.recall_v([sim_x,sim_y],[])
[est_krig var_krig]= krig([x y],z,[sim_x sim_y],'1 Sph(10)');
est_krig
var_krig = (var_krig.*est_krig).^2
