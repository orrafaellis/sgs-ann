function [CC,MAE,MBE,RMSE] = performance_statistics(estimated,true)
    CC =  sum((true-mean(true)).*(estimated-mean(estimated))) / ( std(estimated) * std(true) * numel(true)); 
    MAE = sum(abs(estimated-true)) / numel(true);
    MBE = sum(estimated-true)/numel(true);
    RMSE = sqrt(sum((true-estimated).^2)/numel(true));
end
