import sgems

sgems.execute('DeleteObjects SEC')
sgems.execute('DeleteObjects finished')
sgems.execute('NewCartesianGrid  SEC::50::30::1::1::1::1::0::0::0')
sgems.execute('DeleteObjects finished')
sgems.execute('RunGeostatAlgorithm  kriging::/GeostatParamUtils/XML::<parameters>    <algorithm name="kriging" />    <Grid_Name value="SEC" />    <Property_Name value="kriging" />    <Kriging_Type type="Simple Kriging (SK)">        <parameters mean="0" />    </Kriging_Type>    <do_block_kriging value="0" />    <npoints_x value="5" />    <npoints_y value="5" />    <npoints_z value="5" />    <Hard_Data grid="OBS" property="DATA" />    <Search_Ellipsoid value="10000 10000 10000 0 0 0" />    <Min_Conditioning_Data value="0" />    <Max_Conditioning_Data value="12" />    <Variogram nugget="0.15" structures_count="1">        <structure_1 contribution="0.85" type="Spherical">            <ranges max="1500" medium="1500" min="1500" />            <angles x="0" y="0" z="0" />        </structure_1>    </Variogram>    <Nb_Realizations value="9" /></parameters>')


sgems.execute('SaveGeostatGrid  SEC::kriging.out::gslib::0::kriging__real0::kriging__real1::kriging__real2::kriging__real3::kriging__real4::kriging__real5::kriging__real6::kriging__real7::kriging__real8')
sgems.execute('SaveGeostatGrid  SEC::kriging.sgems::s-gems::0::kriging__real0::kriging__real1::kriging__real2::kriging__real3::kriging__real4::kriging__real5::kriging__real6::kriging__real7::kriging__real8')


sgems.execute('NewCartesianGrid  finished::1::1::1::1.0::1.0::1.0::0::0::0')
data=[]
data.append(1)
sgems.set_property('finished','dummy',data)
sgems.execute('SaveGeostatGrid  finished::finished::gslib::0::dummy')
