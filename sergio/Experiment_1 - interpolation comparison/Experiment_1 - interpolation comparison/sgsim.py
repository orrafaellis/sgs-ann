import sgems

sgems.execute('DeleteObjects SIM')
sgems.execute('DeleteObjects finished')
sgems.execute('NewCartesianGrid  SIM::70::60::1::1::1::1::0::0::0')
sgems.execute('DeleteObjects finished')
sgems.execute('RunGeostatAlgorithm  sgsim::/GeostatParamUtils/XML::<parameters>    <algorithm name="sgsim" />    <Grid_Name value="SIM" />    <Property_Name value="SGSIM" />    <Nb_Realizations value="12" />    <Seed value="14071789" />    <Kriging_Type value="Simple Kriging (SK)" />    <Trend value="0 0 0 0 0 0 0 0 0" />    <Local_Mean_Property value="" />    <Assign_Hard_Data value="1" />    <Hard_Data grid="" property="" />    <Max_Conditioning_Data value="28" />    <Search_Ellipsoid value="80 80 80 0 0 0" />    <Use_Target_Histogram value="0" />    <nonParamCdf ref_on_file="0" ref_on_grid="1" break_ties="0" filename="" grid="" property="">        <LTI_type function="Power" extreme="0" omega="3" />        <UTI_type function="Power" extreme="0" omega="0.333" />    </nonParamCdf>    <Variogram nugget="0.001" structures_count="1">        <structure_1 contribution="0.999" type="Gaussian">            <ranges max="40" medium="20" min="13" />            <angles x="80" y="0" z="0" />        </structure_1>    </Variogram></parameters>')


sgems.execute('SaveGeostatGrid  SIM::SGSIM.out::gslib::0::SGSIM__real0::SGSIM__real1::SGSIM__real2::SGSIM__real3::SGSIM__real4::SGSIM__real5::SGSIM__real6::SGSIM__real7::SGSIM__real8::SGSIM__real9::SGSIM__real10::SGSIM__real11')
sgems.execute('SaveGeostatGrid  SIM::SGSIM.sgems::s-gems::0::SGSIM__real0::SGSIM__real1::SGSIM__real2::SGSIM__real3::SGSIM__real4::SGSIM__real5::SGSIM__real6::SGSIM__real7::SGSIM__real8::SGSIM__real9::SGSIM__real10::SGSIM__real11')


sgems.execute('NewCartesianGrid  finished::1::1::1::1.0::1.0::1.0::0::0::0')
data=[]
data.append(1)
sgems.set_property('finished','dummy',data)
sgems.execute('SaveGeostatGrid  finished::finished::gslib::0::dummy')
