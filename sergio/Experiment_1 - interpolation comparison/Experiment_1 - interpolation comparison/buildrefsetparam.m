%This function estimates the local PDF given some grid and a big dataset,
%  where the grid was extracted from. It builds the local PDFs based on the
%  closest K points of each point in the grid and return mean 'm' and variance 'v'
%For each point in the grid, it finds the 'K' closest neighbors and return
%  their mean and variance of them.
%Dataset is a Nx3 matrix where each row is 'x,y,v'. 'x' and 'y' are
%  coordinates and 'v' is the variable under estimation 
%  (i.e. the one which local mean and variance will be returned)
%'dataset' is a big set of points where the neighbors will be searched from
%'K' is the number of neighbors
function [m v] = buildrefsetparam(grid,dataset,K)
    if K < 1
        error('Number of neighbors qty must be greater or equal to 1');
    end
    if K > size(dataset,1)
        error('Dataset size must be greater than qty');
    end
    if size(dataset,2) ~= 3
        error('Dataset must have 3 columns x,y,v');
    end
    
    for i=1:size(grid,1)
        [indexes] = findneighbors(grid(i,1:2),dataset(:,1:2),K);
        m(i) = mean(dataset(indexes,3));
        v(i) = var(dataset(indexes,3));
    end
end