% INVERSE DISTANCE WEIGHT
% INPUTS
    %row vectors with known locations and values.
    %row vectors with locations to estimate
    %power = distance weight
    %fr_ng --- 'fr' = fixed radius ;  'ng' = neighbours
    %rd_nn --- radius lenght if fr_ng == 'fr' / number of neighbours if  fr_ng =='ng'
%%% OUTPUTS
    %idw_est = row vector with the interpolated values in each (x_est,y_est)
    %location
function [idw_est] = idw(x_known,y_known,z_known,x_est,y_est,power,fr_ng,rd_nn)
if  strcmp(fr_ng,'fr')
    if  (rd_nn<=0)
        disp('Error: Radius must be positive')
        return
    end
    for i=1:length(x_est)
%        for j=1:length(y)
            D=[]; V=[]; wV =[]; vcc=[];
            D= sqrt((x_est(i)-x_known).^2 +(y_est(i)-y_known).^2);
            if min(D)==0
                disp('Error: One or more stations have the coordinates of an interpolation point')
                return
            end
            vcc=z_known(D<rd_nn); D=D(D<rd_nn); 
            V = vcc.*(D.^power);
            wV = D.^power;
            if isempty(D)
                V=NaN;
            else
                V=sum(V)/sum(wV);
            end
%            Vint(j,i)=V;
            idw_est(i,1)=V;
%        end
    end
else
    if (rd_nn > length(z_known)) || (rd_nn<1)
        disp('Error: Number of neighbours not congruent with data')
        return
    end
    for i=1:length(x_est)
        %for j=1:length(y)
            D=[]; V=[]; wV =[];vcc=[];
            D=sqrt((x_est(i)-x_known).^2 +(y_est(i)-y_known).^2);
            if min(D)==0
                disp('Error: One or more stations have the coordinates of an interpolation point')
                return
            end
            [D,I]=sort(D);
            vcc=z_known(I);
            V = vcc(1:rd_nn).*(D(1:rd_nn).^power);
            wV = D(1:rd_nn).^power;
            V=sum(V)/sum(wV);
            %Vint(j,i)=V;
            idw_est(i,1)=V;
        %end
    end
end

