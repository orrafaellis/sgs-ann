%This function finds the 'K' closest neighbors for each point in the grid
%  and return them
%'dataset' is a Nx3 matrix where each row is 'x,y,v'. 'x' and 'y' are
%  coordinates and 'v' is the variable under estimation 
%  (i.e. the one which values will be returned)
%'dataset' is a big set of points where the neighbors will be searched from
%'K' is the number of neighbors
function [neighbors] = buildrefset(grid,dataset,K)
    if K < 1
        error('Number of neighbors qty must be greater or equal to 1');
    end
    if K > size(dataset,1)
        error('Dataset size must be greater than qty');
    end
    if size(dataset,2) ~= 3
        error('Dataset must have 3 columns x,y,v');
    end
    if size(grid,2) < 2
        error('Grid must have at least 2 columns (x,y)');
    end
    
    neighbors = zeros(size(grid,1),K);
    
    for i=1:size(grid,1)
        [indexes] = findneighbors(grid(i,1:2),dataset(:,1:2),K);
        neighbors(i,:) = dataset(indexes,3);
    end
end