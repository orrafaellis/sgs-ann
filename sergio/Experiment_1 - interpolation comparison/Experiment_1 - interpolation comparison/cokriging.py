import sgems

sgems.execute('DeleteObjects SEC')
sgems.execute('DeleteObjects finished')
sgems.execute('DeleteObjects finished')
sgems.execute('RunGeostatAlgorithm  cokriging::/GeostatParamUtils/XML::<parameters>    <algorithm name="cokriging" />    <Grid_Name value="SEC" />    <Property_Name value="cokriging" />    <Kriging_Type value="Simple Kriging (SK)" />    <SK_Means value="0.570788 8.03788e+06" />    <Cokriging_Type value="Markov Model 2" />    <Primary_Harddata_Grid value="OBS" />    <Primary_Variable value="DATA" />    <Assign_Hard_Data value="0" />    <Secondary_Harddata_Grid value="SEC" />    <Secondary_Variable value="SECDATA" />    <Min_Conditioning_Data value="0" />    <Max_Conditioning_Data value="20" />    <Search_Ellipsoid_1 value="10000 10000 10000 0 0 0" />    <Min_Conditioning_Data_2 value="0" />    <Max_Conditioning_Data_2 value="20" />    <Search_Ellipsoid_2 value="0 0 0 0 0 0" />    <Variogram_C11 nugget="0.154746" structures_count="1">        <structure_1 contribution="0.517054" type="Spherical">            <ranges max="1540.55" medium="1540.55" min="1540.55" />            <angles x="0" y="0" z="0" />        </structure_1>    </Variogram_C11>    <Variogram_C12 nugget="0" structures_count="1">        <structure_1 contribution="0" type="Spherical">            <ranges max="0" medium="0" min="0" />            <angles x="0" y="0" z="0" />        </structure_1>    </Variogram_C12>    <Variogram_C22 nugget="0" structures_count="1">        <structure_1 contribution="0" type="Spherical">            <ranges max="0" medium="0" min="0" />            <angles x="0" y="0" z="0" />        </structure_1>    </Variogram_C22>    <Correl_Z1Z2 value="-0.764395" />    <Var_Z2 value="1100000000000" />    <MM2_Correl_Z1Z2 value="-0.8" />    <MM2_Variogram_C22 nugget="0" structures_count="1">        <structure_1 contribution="750000000000" type="Spherical">            <ranges max="1800" medium="1800" min="1800" />            <angles x="0" y="0" z="0" />        </structure_1>    </MM2_Variogram_C22>    <Nb_Realizations value="9" /></parameters>')


sgems.execute('SaveGeostatGrid  SEC::cokriging.out::gslib::0::cokriging__real0::cokriging__real1::cokriging__real2::cokriging__real3::cokriging__real4::cokriging__real5::cokriging__real6::cokriging__real7::cokriging__real8')
sgems.execute('SaveGeostatGrid  SEC::cokriging.sgems::s-gems::0::cokriging__real0::cokriging__real1::cokriging__real2::cokriging__real3::cokriging__real4::cokriging__real5::cokriging__real6::cokriging__real7::cokriging__real8')


sgems.execute('NewCartesianGrid  finished::1::1::1::1.0::1.0::1.0::0::0::0')
data=[]
data.append(1)
sgems.set_property('finished','dummy',data)
sgems.execute('SaveGeostatGrid  finished::finished::gslib::0::dummy')
