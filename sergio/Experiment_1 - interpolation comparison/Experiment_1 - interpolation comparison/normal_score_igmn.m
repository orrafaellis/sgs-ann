%testing normal score transform with IGMN
%compares performance of estimation between IGMN-over-normal-data 
%and IGMN-over-original-data

clear all; clc;

trainingSetSize = 300;
testSetSize = 170;

datasetPath='datasets/walker/walker_lake_sample_dataset.csv';
fprintf('Loading dataset: %s ...\n',datasetPath);
ds = dataset('File',datasetPath,'Delimiter','\t');

%Prints dataset summary
summary(ds)
x = ds.x;
y = ds.y;
z = ds.v; %chooses variable for interpolation

% Builds TestSet and TrainingSet
% Randomly removes 'testSetSize' samples from the dataset to use for testing
fprintf('Constructing random Test and Training Sets. \nDataset size = %d  Test Set size = %d  Training Set size = %d \n Press any key to continue...\n',numel(x),testSetSize,trainingSetSize);
testSetIndex = randsample(numel(x),testSetSize);
testSet = dataset({x(testSetIndex),'x'},{y(testSetIndex),'y'},{z(testSetIndex),'z'}); %builds test set
x(testSetIndex) = []; %remove test set from original dataset
y(testSetIndex) = [];
z(testSetIndex) = [];

%trainingSet = dataset({x,'x'},{y,'y'},{z,'z'}); %Builds training set
trainingSetIndex = randsample(numel(x),trainingSetSize);
trainingSet = dataset({x(trainingSetIndex),'x'},{y(trainingSetIndex),'y'},{z(trainingSetIndex),'z'}); %builds test set
fprintf('\nTest set Summary: \n');
summary(testSet);
fprintf('\nTraining set summary: \n');
summary(trainingSet);
fprintf('\nTest and Training set successfuly constructed! Summary presented above...\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%SIIGMN-original without normal score
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('\n\nTesting SIIGMN-original...\n');
options = [];
options.Display = 'off';

%Options specific for SIIGMN
%trend = mean(trainingSet.z);
data = [trainingSet.x trainingSet.y trainingSet.z];
delta = 0.1;
range = max(data)-min(data);
model = siigmn('range', range,'CovType', 'full', 'tau', 0.05, 'delta', delta, 'vmin', 0, 'spmin', 0, 'covAngle',14, 'Options', options);
model = model.train(data);%trains the model
est_siigmn = model.recall([testSet.x testSet.y],[]);
%performance of SIIGMN
[siigmn_CC, siigmn_MAE, siigmn_MBE, siigmn_RMSE] = performance_statistics(est_siigmn,testSet.z);
fprintf('CC\tMAE\tMBE\tRMSE\t[SIIGMN]\n');
fprintf('%.2f\t%.2f\t%.2f\t%.2f\n',siigmn_CC,siigmn_MAE,siigmn_MBE,siigmn_RMSE);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%SIIGMN-normal - with normal score transform
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('\n\nTesting SIIGMN-normalized...\n');
options = [];
options.Display = 'off';

[z_nscore o_nscore] = nscore(trainingSet.z);

data = [trainingSet.x trainingSet.y z_nscore];
delta = 0.1;
range = max(data)-min(data);
model = siigmn('range', range,'CovType', 'full', 'tau', 0.05, 'delta', delta, 'vmin', 0, 'spmin', 0, 'covAngle',14, 'Options', options);
model = model.train(data);%trains the model
est_siigmn_2 = model.recall([testSet.x testSet.y],[]);
%performance of SIIGMN
est_inscore = inscore(est_siigmn_2,o_nscore);
[siigmn_CC, siigmn_MAE, siigmn_MBE, siigmn_RMSE] = performance_statistics(est_inscore,testSet.z);
fprintf('CC\tMAE\tMBE\tRMSE\t[SIIGMN]\n');
fprintf('%.2f\t%.2f\t%.2f\t%.2f\n',siigmn_CC,siigmn_MAE,siigmn_MBE,siigmn_RMSE);


