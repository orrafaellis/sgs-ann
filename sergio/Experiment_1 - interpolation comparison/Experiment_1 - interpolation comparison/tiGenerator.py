import sgems

sgems.execute('DeleteObjects SIM')
sgems.execute('DeleteObjects finished')
sgems.execute('NewCartesianGrid  SIM::50::30::1::1::1::1::0::0::0')
sgems.execute('DeleteObjects finished')
sgems.execute('RunGeostatAlgorithm  tiGenerator::/GeostatParamUtils/XML::<parameters>    <algorithm name="tiGenerator" />    <Ti_grid value="SIM" />    <Ti_prop_name value="TIG" />    <nb_realizations value="5" />    <seed_rand value="52817" />    <nb_geobodies value="1" />    <geobodySelector index="1" prop="0.2" type="Sinusoid">        <sinRot cdf_type="Constant">            <const mean="25" />        </sinRot>        <sinLen cdf_type="Uniform">            <unif min="80" max="100" />        </sinLen>        <sinWid cdf_type="Uniform">            <unif min="6" max="8" />        </sinWid>        <sinThk cdf_type="Uniform">            <unif min="2" max="4" />        </sinThk>        <sinAmp cdf_type="Uniform">            <unif min="4" max="8" />        </sinAmp>        <sinWvl cdf_type="Uniform">            <unif min="20" max="40" />        </sinWvl>        <interaction erosion="">            <overlap min="" max="" />            <flag no_self_overlap="1" />        </interaction>    </geobodySelector>    <Nb_Realizations value="9" /></parameters>')


sgems.execute('SaveGeostatGrid  SIM::TIG.out::gslib::0::TIG__real0::TIG__real1::TIG__real2::TIG__real3::TIG__real4::TIG__real5::TIG__real6::TIG__real7::TIG__real8')
sgems.execute('SaveGeostatGrid  SIM::TIG.sgems::s-gems::0::TIG__real0::TIG__real1::TIG__real2::TIG__real3::TIG__real4::TIG__real5::TIG__real6::TIG__real7::TIG__real8')


sgems.execute('NewCartesianGrid  finished::1::1::1::1.0::1.0::1.0::0::0::0')
data=[]
data.append(1)
sgems.set_property('finished','dummy',data)
sgems.execute('SaveGeostatGrid  finished::finished::gslib::0::dummy')
