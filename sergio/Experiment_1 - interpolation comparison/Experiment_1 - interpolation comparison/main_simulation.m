%list of things pending:
%1 - find a bigger dataset (ok)
%2 - do 1 test simulation (ok)
%3 - do sequencial simulation 
% *** Normal F score transform
%4 - plot 1 realization of real, krig and igmn
%5 - plot histogram of simulation of krig and igmn
%6 - print statistics (mean, std) of krig and igmn simulations
%7 - implement and print discrepancy statistics using komolgorov smirnov
%and acceptance rate of each method IGMN and krig

clc; clear all;

%main parameters
datasetPath='datasets/walker/walker_lake_sample_dataset.csv';
bigDatasetPath='datasets/walker/Exhaustive_set.dat';
trainingSetSize = [200];
simulationSetSize = 30;
realizations = 200;
K = 300;
alpha=0.05; %ks test significance level
run_sgs=0; 

variogram='22000 Nug(0) + 40000 Sph(25,14,.83) + 45000 Sph(50,14,0.33)'; %Variogram model for kriging
printControlMsg=0;
printEachRunResults=0;
pauseAfterEachRun=0;
pauseAfterEachExp=0;
plotBubble=0;

for exp=1:numel(trainingSetSize) %test with different training set size [25 50 75 and 100]
        %Loads dataset
        if(printControlMsg)
            fprintf('Loading dataset: %s ...\n',datasetPath);
        end
        ds = dataset('File',datasetPath,'Delimiter','\t');

         
        %Prints dataset summary
        if(printControlMsg)
            summary(ds)
        end
        x = ds.x;
        y = ds.y;
        z = ds.v; %chooses variable for interpolation

        % Builds simulationSet and trainingSet
        
        % Randomly removes 'simulationSetSize' samples from the dataset to use for simulation
        if(printControlMsg)
            fprintf('Constructing random Simulation and Training Sets. \nDataset size = %d  Simulation Set size = %d  Training Set size = %d \n Press any key to continue...\n',numel(x),simulationSetSize,trainingSetSize(exp));
        end
        simulationSetIndex = randsample(numel(x),simulationSetSize);
        simulationSet = dataset({x(simulationSetIndex),'x'},{y(simulationSetIndex),'y'},{z(simulationSetIndex),'z'}); %builds test set
        x(simulationSetIndex) = []; %remove simulation set from original dataset
        y(simulationSetIndex) = [];
        z(simulationSetIndex) = [];
        
        %trainingSet = dataset({x,'x'},{y,'y'},{z,'z'}); %Builds training set
        trainingSetIndex = randsample(numel(x),trainingSetSize(exp));
        trainingSet = dataset({x(trainingSetIndex),'x'},{y(trainingSetIndex),'y'},{z(trainingSetIndex),'z'}); %builds training set
        if(printControlMsg)
            fprintf('\nSimulation set Summary: \n');
            summary(simulationSet);
            fprintf('\nTraining set summary: \n');
            summary(trainingSet);
            fprintf('\nSimulation and Training set successfuly constructed! Summary presented above...');
        end
        
        if(plotBubble)
            %Plots dataset bubble plot
            subplot(2,3,1)
            if(printControlMsg)
                fprintf('Plotting bubble plot for dataset...\n\n');
            end
            scatter(trainingSet.x,trainingSet.y,trainingSet.z);
            hold on
            scatter(simulationSet.x,simulationSet.y,simulationSet.z,'red');
            title('Walker Lake sample dataset');
            xlabel('Longitude');
            ylabel('Latitude');
            legend('Random training set','Random simulation set');
            %plotSurface(x,y,z);
        end       
        plotSurface(x,y,z);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %Reference Set
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        fprintf('\nBuilding reference set...\n');
        exhaustiveSet = dataset('File',bigDatasetPath,'Delimiter',',');
        
        refset = buildrefset([simulationSet.x simulationSet.y],[exhaustiveSet.X exhaustiveSet.Y exhaustiveSet.V],K);
        refset_mean = mean(refset,2);
        refset_var  = var(refset')';               
        

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %KRIGING test
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if(printControlMsg)
            fprintf('\n\nTesting Kriging...\n');
        end

%         [est_krig,var_krig] = krig([trainingSet.x trainingSet.y],trainingSet.z,[simulationSet.x simulationSet.y],variogram);
%         
%         if(plotBubble)
%             subplot(2,3,2)
%             scatter(simulationSet.x,simulationSet.y,simulationSet.z,'blue');
%             hold on
%             scatter(simulationSet.x,simulationSet.y,est_krig,'red');
%             title('SimulationSet - True vs Estimated (Kriging)');
%             xlabel('Longitude');
%             ylabel('Latitude');
%             legend('True','Estimated');
%             hold off;
%         end
%         
%         %performance of Kriging
%         [krig_CC krig_MAE krig_MBE krig_RMSE] = performance_statistics(est_krig,simulationSet.z);
%         %if(printControlMsg)
%             fprintf('CC\tMAE\tMBE\tRMSE\t[Kriging]\n');
%             fprintf('%.2f\t%.2f\t%.2f\t%.2f\n',krig_CC,krig_MAE,krig_MBE,krig_RMSE);
%         %end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %IGMN - interpolation with normal score transform
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if(printControlMsg)
            fprintf('\n\nTesting IGMN...\n');
        end
        options = [];
        options.Display = 'off';

        %Options specific for IGMN
        %trend = mean(trainingSet.z);
        
        %[z_nscore o_nscore] = nscore(trainingSet.z); %normal score transform (only necessary on simulation but I included here for testing)
        %data = [trainingSet.x trainingSet.y z_nscore];
        
        
        data = [trainingSet.x trainingSet.y trainingSet.z];
        delta = 0.1;
        range = max(data)-min(data);
        
        model = siigmn('range', range,'CovType', 'full', 'tau', 0.05, 'delta', delta, 'vmin', 0, 'spmin', 0, 'covAngle',14, 'Options', options);

        model = model.train(data);%trains the model
        
        %[est_siigmn_normal var_siigmn]= model.recall_v([simulationSet.x simulationSet.y],[]);
        [est_siigmn var_siigmn]= model.recall_v([simulationSet.x simulationSet.y],[]);
        
        var_siigmn = reshape(var_siigmn,1,simulationSetSize)';
        
        %est_siigmn = inscore(est_siigmn_normal,o_nscore); %inverse normal score transform
      
        if(plotBubble)
            subplot(2,3,5)
            scatter(simulationSet.x,simulationSet.y,simulationSet.z,'blue');
            hold on
            scatter(simulationSet.x,simulationSet.y,est_siigmn,'red');
            title('SimulationSet - True vs Estimated (SIIGMN)');
            xlabel('Longitude');
            ylabel('Latitude');
            legend('True','Estimated');
            hold off;
        end
        %performance of SIIGMN
        [siigmn_CC, siigmn_MAE, siigmn_MBE, siigmn_RMSE] = performance_statistics(est_siigmn,simulationSet.z);

        fprintf('CC\tMAE\tMBE\tRMSE\t[SIIGMN]\n');
        fprintf('%.2f\t%.2f\t%.2f\t%.2f\n',siigmn_CC,siigmn_MAE,siigmn_MBE,siigmn_RMSE);

        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %SGS - Sequential Gaussian Simulation
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         x = [1 2 3 4 5 6 7 8 9 10]';
%         y = x;
%         z = [11 12 13 14 15 16 17 18 19 20]';
%         sim_x = [1.5 4.5 8.5]';
%         sim_y = sim_x;
%         variogram = '1 Sph(3)';
%         sim = sgsim([x y],z,[sim_x sim_y],variogram,options);
        

    if(run_sgs)
            options.nsim = realizations;
            sg_sim = sgsim([trainingSet.x trainingSet.y],trainingSet.z,[simulationSet.x simulationSet.y],variogram,options);
            sg_sim_mean = mean(sg_sim,2);
            sg_sim_var = var(sg_sim')';

            %KS Test - Kolmogorov-Smirnov
            ks_sgs = zeros(simulationSetSize,1);
            for i=1:simulationSetSize
                ks_sgs(i,1) = kstest2(refset(i,:),sg_sim(i,:),'Alpha',alpha);
            end
    end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %IGMN Simulation
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        options = [];
        options.Display = 'off';
        %trend = mean(trainingSet.z);
        
        [z_nscore o_nscore] = nscore(trainingSet.z); %normal score transform (only necessary on simulation but I included here for testing)
        data = [trainingSet.x trainingSet.y z_nscore];
        %data
        %data = [trainingSet.x trainingSet.y trainingSet.z];
        delta = 0.1;
        range = max(data)-min(data);
        
        igmn_model = siigmn('range', range,'CovType', 'full', 'tau', 0.05, 'delta', delta, 'vmin', 0, 'spmin', 0, 'covAngle',14, 'Options', options);
        
        igmn_model = igmn_model.train(data);%trains the model
        igmn_sim = igmn_model.simulate([simulationSet.x simulationSet.y],realizations);
       
        for ss=1:size(igmn_sim,2) %inverse normal score transform
            igmn_sim(:,ss) = inscore(igmn_sim(:,ss),o_nscore);
        end
        
        igmn_sim_mean = mean(igmn_sim,2);
        igmn_sim_var = var(igmn_sim')';
        
        %KS Test - Kolmogorov-Smirnov
        ks_igmn = zeros(simulationSetSize,1);
        for i=1:simulationSetSize
            ks_igmn(i,1) = kstest2(refset(i,:),igmn_sim(i,:),'Alpha',alpha);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %Print Results
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        %Full table, including SGS
        %T = table(est_krig,est_siigmn,sg_sim_mean,igmn_sim_mean,...
        %    var_krig,var_siigmn,sg_sim_var,igmn_sim_var,...
        %    'VariableNames',{'Krig_est','IGMN_est','SGS_mean','IGMNsim_mean','Krig_var','IGMN_var','SGS_var','IGMNsim_var'})
        
        
        igmn_rate = 100-(sum(ks_igmn)/simulationSetSize)*100;
        if(run_sgs)
            %Table with SGS
            T = table(refset_mean,est_krig,est_siigmn,igmn_sim_mean,sg_sim_mean,...
                refset_var,var_krig,var_siigmn,igmn_sim_var,sg_sim_var,ks_igmn,ks_sgs,...
                'VariableNames',{'RefSet_Mean','Krig_est','IGMN_est',...
                'IGMNsim_mean','SGS_mean','RefSet_Var','Krig_var','IGMN_var','IGMNsim_var','SGS_var','KS_igmn','KS_SGS'})
            sgs_rate = 100-(sum(ks_sgs)/simulationSetSize)*100;
            fprintf('\nSGS KS test rate: %.1f',sgs_rate);
        else
            %Table without SGS
            T = table(refset_mean,est_krig,est_siigmn,igmn_sim_mean,...
                refset_var,var_krig,var_siigmn,igmn_sim_var,ks_igmn,...
                'VariableNames',{'RefSet_Mean','Krig_est','IGMN_est',...
                'IGMNsim_mean','RefSet_Var','Krig_var','IGMN_var','IGMNsim_var','KS_igmn'})
        end
        fprintf('\nIGMN KS test rate: %.1f',igmn_rate);
            
end




