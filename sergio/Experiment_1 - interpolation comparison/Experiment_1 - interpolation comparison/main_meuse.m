clc; clear all;

%main parameters
datasetPath='datasets/meuse/meuse.txt';
testSetSize = 55;
numberOfRuns = 30;
trainingSetSize = [25 50 75 100];
%variogram='0.05 Nug(0) + 0.59 Sph(600,-45,.8)'; %Variogram model for kriging
%variogram='0.05 Nug(0) + 0.59 Sph(1200,45,.4)'; %Variogram model for kriging
%variogram = '0.7 Gau(120)'
variogram='0.05 Nug(0) + 0.59 Sph(120,45,.4)';
idw_power = 2; %IDW power
mlp_min_size = 3;
mlp_max_size = 7;
mlp_repeat_train = 1;
siigmn_variogram_sill=0.59;
printControlMsg=0;
printEachRunResults=0;
pauseAfterEachRun=0;
pauseAfterEachExp=0;

for exp=1:numel(trainingSetSize) %test with different training set size [25 50 75 and 100]
    for run=1:numberOfRuns

        %Loads dataset
        if(printControlMsg)
            fprintf('Loading dataset: %s ...\n',datasetPath);
        end
        ds = dataset('File',datasetPath,'Delimiter',',');

         
        %Prints dataset summary
        if(printControlMsg)
            summary(ds)
        end
        x = ds.x;
        y = ds.y;
        z = ds.zinc; %chooses variable for interpolation

        % Builds TestSet and TrainingSet
        % Randomly removes 'testSetSize' samples from the dataset to use for testing
        if(printControlMsg)
            fprintf('Constructing random Test and Training Sets. \nDataset size = %d  Test Set size = %d  Training Set size = %d \n Press any key to continue...\n',numel(x),testSetSize,trainingSetSize(exp));
        end
        testSetIndex = randsample(numel(x),testSetSize);
        testSet = dataset({x(testSetIndex),'x'},{y(testSetIndex),'y'},{z(testSetIndex),'z'}); %builds test set
        x(testSetIndex) = []; %remove test set from original dataset
        y(testSetIndex) = [];
        z(testSetIndex) = [];
        
        %trainingSet = dataset({x,'x'},{y,'y'},{z,'z'}); %Builds training set
        trainingSetIndex = randsample(numel(x),trainingSetSize(exp));
        trainingSet = dataset({x(trainingSetIndex),'x'},{y(trainingSetIndex),'y'},{z(trainingSetIndex),'z'}); %builds test set
        if(printControlMsg)
            fprintf('\nTest set Summary: \n');
            summary(testSet);
            fprintf('\nTraining set summary: \n');
            summary(trainingSet);
            fprintf('\nTest and Training set successfuly constructed! Summary presented above...');
        end
        %Plots dataset bubble plot
        subplot(2,3,1)
        if(printControlMsg)
            fprintf('Plotting bubble plot for Meuse River dataset...\n\n');
        end
        scatter(trainingSet.x,trainingSet.y,trainingSet.z);
        hold on
        scatter(testSet.x,testSet.y,testSet.z,'red');
        title('Meuse River dataset');
        xlabel('Longitude');
        ylabel('Latitude');
        legend('Random training set','Random test set');
        %plotSurface(x,y,z);


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %KRIGING test
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if(printControlMsg)
            fprintf('\n\nTesting Kriging...\n');
        end
        [est_krig,var_krig] = krig([trainingSet.x trainingSet.y],trainingSet.z,[testSet.x testSet.y],variogram);
        
        %[est_krig,var_krig] = krig_test(variogram,trainingSet.x,trainingSet.y,trainingSet.z,testSet.x,testSet.y);
        %plot(pos_est,d_est,'k',pos_known,val_known,'ro')
        subplot(2,3,2)
        scatter(testSet.x,testSet.y,testSet.z,'blue');
        hold on
        scatter(testSet.x,testSet.y,est_krig,'red');
        title('TestSet - True vs Estimated (Kriging)');
        xlabel('Longitude');
        ylabel('Latitude');
        legend('True','Estimated');
        hold off;
        %performance of Kriging
        [krig_CC(run) krig_MAE(run) krig_MBE(run) krig_RMSE(run)] = performance_statistics(est_krig,testSet.z);
        if(printControlMsg)
            fprintf('CC\tMAE\tMBE\tRMSE\t[Kriging]\n');
            fprintf('%.2f\t%.2f\t%.2f\t%.2f\n',krig_CC(run),krig_MAE(run),krig_MBE(run),krig_RMSE(run));
        end


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %IDW test
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if(printControlMsg)
            fprintf('\n\nTesting IDW...\n');
        end
        [est_idw]=idw(trainingSet.x, trainingSet.y, trainingSet.z,testSet.x, testSet.y,idw_power,'nn',10);
        subplot(2,3,3)
        scatter(testSet.x,testSet.y,testSet.z,'blue');
        hold on
        scatter(testSet.x,testSet.y,est_idw,'red');
        title('TestSet - True vs Estimated (IDW)');
        xlabel('Longitude');
        ylabel('Latitude');
        legend('True','Estimated');
        hold off;
        %performance of IDW
        [idw_CC(run) idw_MAE(run) idw_MBE(run) idw_RMSE(run)] = performance_statistics(est_idw,testSet.z);
        if(printControlMsg)
            fprintf('CC\tMAE\tMBE\tRMSE\t[IDW]\n');
            fprintf('%.2f\t%.2f\t%.2f\t%.2f\n',idw_CC(run),idw_MAE(run),idw_MBE(run),idw_RMSE(run));
        end


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %IGMN test
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if(printControlMsg)
            fprintf('\nTesting IGMN...\n');
        end
        options = [];
        options.Display = 'off';
        delta = 0.06;
        data = [trainingSet.x trainingSet.y trainingSet.z];
        range = max(data)-min(data);
        model = igmn('range', range,'CovType', 'full', 'tau', 0.05, 'delta', delta, 'vmin', 0, 'spmin', 0, 'Options', options);
        %trains the model
        model = model.train(data);
        est_igmn = model.recall([testSet.x testSet.y]);
        %plots bubbleplot with results
        subplot(2,3,4)
        scatter(testSet.x,testSet.y,testSet.z,'blue');
        hold on
        scatter(testSet.x,testSet.y,est_igmn,'red');
        title('TestSet - True vs Estimated (IGMN)');
        xlabel('Longitude');
        ylabel('Latitude');
        legend('True','Estimated');
        hold off;
        %performance of IGMN
        [igmn_CC(run) igmn_MAE(run) igmn_MBE(run) igmn_RMSE(run)] = performance_statistics(est_igmn,testSet.z);
        if(printControlMsg)
            fprintf('CC\tMAE\tMBE\tRMSE\t[IGMN]\n');
            fprintf('%.2f\t%.2f\t%.2f\t%.2f\n',igmn_CC(run),igmn_MAE(run),igmn_MBE(run),igmn_RMSE(run));
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %SIIGMN with variance
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if(printControlMsg)
            fprintf('\n\nTesting SIIGMN-var...\n');
        end
        options = [];
        options.Display = 'off';

        %Options specific for SIIGMN
        trend = mean(trainingSet.z);
        data = [trainingSet.x trainingSet.y trainingSet.z];
        delta = 0.06;
        range = max(data)-min(data);    
        model = siigmn('range', range,'CovType', 'full', 'tau', 0.05, 'delta', delta, 'vmin', 0, 'spmin', 0, 'covAngle',-45, 'Options', options);

        model = model.train(data);%trains the model
        %est_siigmn = model.recall_variogram([testSet.x testSet.y],trend,siigmn_variogram_sill,@meuse_variogram);

        %est_siigmn = model.recall([testSet.x testSet.y]);
        [est_siigmn_v var_siigmn_v]= model.recall_v([testSet.x testSet.y],trend);
       
        %imprime variancia da krigagem e igmn
%         fprintf('\nSIIGMN-v\tKrig\t\tvar_siigmn\tvar_krig\treal\n');
%         for k=1:numel(est_siigmn_v)
%             fprintf('%.1f\t\t%.1f\t\t%.0f\t\t%.0f\t\t%.0f\n',est_siigmn(k),est_krig(k),var_siigmn_v(k),(var_krig(k)*est_krig(k))^2,testSet.z(k));
%         end
%         vari = reshape(var_siigmn_v,[1 55])';
%         vark = (var_krig.*est_krig).^2;
%         figure;
%         plotregression(vari,vark); %plots variancia da krigagem e igmn
%         pause;
%         
        subplot(2,3,5)
        scatter(testSet.x,testSet.y,testSet.z,'blue');
        hold on
        scatter(testSet.x,testSet.y,est_siigmn_v,'red');
        title('TestSet - True vs Estimated (SIIGMN-var)');
        xlabel('Longitude');
        ylabel('Latitude');
        legend('True','Estimated');
        hold off;
        %performance of SIIGMN
        [siigmn_v_CC(run), siigmn_v_MAE(run), siigmn_v_MBE(run), siigmn_v_RMSE(run)] = performance_statistics(est_siigmn_v,testSet.z);
        if(printControlMsg)
            fprintf('CC\tMAE\tMBE\tRMSE\t[SIIGMN-v]\n');
            fprintf('%.2f\t%.2f\t%.2f\t%.2f\n',siigmn_v_CC(run),siigmn_v_MAE(run),siigmn_v_MBE(run),siigmn_v_RMSE(run));
        end   
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %MLP test
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if(printControlMsg)
            fprintf('\nTesting MLP...\n');
        end
        best_tperf=9999999;
        for i=mlp_min_size:mlp_max_size %trains networks with different architectures (from 3 to 'mlp_neurons' hidden neurons)
            for j=1:mlp_repeat_train %trains the same architecture 'mlp_trains' 
                net = newff([trainingSet.x trainingSet.y]',trainingSet.z',i,{'tansig','tansig'},'trainrp');
                [net, tr] = train(net,[trainingSet.x trainingSet.y]',trainingSet.z');
                if(tr.best_tperf < best_tperf)
                    best_net = net;
                    best_tperf = tr.best_tperf;
                    best_net_neurons = i;
                end
            end
        end
        if(printControlMsg)
            fprintf('Best number of neurons on hidden layer: %d\n',best_net_neurons);
        end
        est_mlp = sim(net,[testSet.x testSet.y]');

        subplot(2,3,6)
        scatter(testSet.x,testSet.y,testSet.z,'blue');
        hold on
        scatter(testSet.x,testSet.y,est_mlp,'red');
        title('TestSet - True vs Estimated (MLP)');
        xlabel('Longitude');
        ylabel('Latitude');
        legend('True','Estimated');
        hold off;
        %performance of MLP
        [mlp_CC(run), mlp_MAE(run), mlp_MBE(run), mlp_RMSE(run)] = performance_statistics(est_mlp',testSet.z);
        if(printControlMsg)
            fprintf('CC\tMAE\tMBE\tRMSE\t[MLP]\n');
            fprintf('%.2f\t%.2f\t%.2f\t%.2f\n',mlp_CC(run),mlp_MAE(run),mlp_MBE(run),mlp_RMSE(run));
        end
        
    end

    fprintf('\nMean values of %d runs with trainingSetSize=%d:\n',numberOfRuns,trainingSetSize(exp));
        %Table with mean results
        T = table([mean(krig_RMSE); mean(idw_RMSE); mean(igmn_RMSE); mean(siigmn_v_RMSE); mean(mlp_RMSE)], ...
                  [std(krig_RMSE);  std(idw_RMSE);  std(igmn_RMSE);  std(siigmn_v_RMSE);  std(mlp_RMSE)], ...
                  [mean(krig_MAE);  mean(idw_MAE);  mean(igmn_MAE);  mean(siigmn_v_MAE);  mean(mlp_MAE)], ...
                  [std(krig_MAE);   std(idw_MAE);   std(igmn_MAE);   std(siigmn_v_MAE);   std(mlp_MAE)], ...
                  [mean(krig_MBE);  mean(idw_MBE);  mean(igmn_MBE);  mean(siigmn_v_MBE);  mean(mlp_MBE)], ...
                  [std(krig_MBE);   std(idw_MBE);   std(igmn_MBE);   std(siigmn_v_MBE);   std(mlp_MBE)], ...
                  [mean(krig_CC);   mean(idw_CC);   mean(igmn_CC);   mean(siigmn_v_CC);   mean(mlp_CC)], ...
                  [std(krig_CC);    std(idw_CC);    std(igmn_CC);    std(siigmn_v_CC);    std(mlp_CC)], ...
        'VariableNames',{'mRMSE','stdRMSE','mMAE','stdMAE','mMBE','stdMBE','mCC','stdCC'},...
        'RowNames',{'Kriging','IDW','IGMN','SIIGMN-v','MLP'})
    
    fileResult = sprintf('result%d_meuse.txt',trainingSetSize(exp));
    writetable(T,fileResult);
    
    if(pauseAfterEachExp)
        fprintf('\nExperiment with trainingSetSize=%d finished. Press any key to continue...',trainingSetSize(exp));
        pause;
    end
end