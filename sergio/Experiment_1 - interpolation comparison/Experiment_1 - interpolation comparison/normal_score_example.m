%testing normal score transform

trainingSetSize = 300;
simulationSetSize = 170;

datasetPath='datasets/walker/walker_lake_sample_dataset.csv';
fprintf('Loading dataset: %s ...\n',datasetPath);
ds = dataset('File',datasetPath,'Delimiter','\t');

%Prints dataset summary
summary(ds)
x = ds.x;
y = ds.y;
z = ds.v; %chooses variable for interpolation

%plots original histogram
subplot(3,1,1)
hist(z)

%Normal score transform
[normal_d,normal_o] = nscore(z);
%plots normal transformed histogram
subplot(3,1,2)
hist(normal_d)

%normal score BACK transform
d=inscore(normal_d,normal_o);
%plots inverse normal score histogram
subplot(3,1,3)
hist(d)
pause;

%sorteia apenas alguns dados para transformar
