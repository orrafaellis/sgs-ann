function plotSurface(x,y,z)
    [XI, YI] = meshgrid((min(x):(max(x)-min(x))/100:max(x)) , (min(y):(max(y)-min(y))/100:max(y)));
    ZI = griddata(x,y,z,XI,YI);
    mesh(XI,YI,ZI), hold on
    plot3(x,y,z,'or'), hold off
end