import sgems

sgems.execute('DeleteObjects SIM')
sgems.execute('DeleteObjects finished')
sgems.execute('NewCartesianGrid  SIM::50::30::1::1::1::1::0::0::0')
sgems.execute('DeleteObjects finished')
sgems.execute('RunGeostatAlgorithm  sisim::/GeostatParamUtils/XML::<parameters>    <algorithm name="sisim" />    <Grid_Name value="SIM" />    <Property_Name value="SISIM" />    <Nb_Realizations value="9" />    <Seed value="14071789" />    <Categorical_Variable_Flag value="1" />    <Nb_Indicators value="2" />    <Marginal_Probabilities value="0.5 0.5" />    <lowerTailCdf function="Power" extreme="-5" omega="3" />    <upperTailCdf function="Power" extreme="5" omega="0.333" />    <Median_Ik_Flag value="1" />    <Full_Ik_Flag value="0" />    <Hard_Data_Grid value="" />    <Hard_Data_Property value="" />    <Assign_Hard_Data value="1" />    <coded_grid value="" />    <coded_props count="0" value="" />    <Max_Conditioning_Data value="12" />    <Search_Ellipsoid value="1000 1000 1000 0 0 0" />    <Variogram_Median_Ik nugget="0" structures_count="1">        <structure_1 contribution="0.6" type="Spherical">            <ranges max="30" medium="30" min="10" />            <angles x="0" y="0" z="0" />        </structure_1>    </Variogram_Median_Ik>    <Variogram_Full_Ik nugget="0" structures_count="1">        <structure_1 contribution="0" type="Spherical">            <ranges max="0" medium="0" min="0" />            <angles x="0" y="0" z="0" />        </structure_1>    </Variogram_Full_Ik>    <Variogram_Full_Ik_2 nugget="0" structures_count="1">        <structure_1 contribution="0" type="Spherical">            <ranges max="0" medium="0" min="0" />            <angles x="0" y="0" z="0" />        </structure_1>    </Variogram_Full_Ik_2></parameters>')


sgems.execute('SaveGeostatGrid  SIM::SISIM.out::gslib::0::SISIM__real0::SISIM__real1::SISIM__real2::SISIM__real3::SISIM__real4::SISIM__real5::SISIM__real6::SISIM__real7::SISIM__real8')
sgems.execute('SaveGeostatGrid  SIM::SISIM.sgems::s-gems::0::SISIM__real0::SISIM__real1::SISIM__real2::SISIM__real3::SISIM__real4::SISIM__real5::SISIM__real6::SISIM__real7::SISIM__real8')


sgems.execute('NewCartesianGrid  finished::1::1::1::1.0::1.0::1.0::0::0::0')
data=[]
data.append(1)
sgems.set_property('finished','dummy',data)
sgems.execute('SaveGeostatGrid  finished::finished::gslib::0::dummy')
