%finds the closest 'K' neighbors to 'refpoint'. Return their indexes on 'dataset' and their euclidean distances
%'refpoint' are the coordinates (x,y) of reference point where we calculate the distance to
%'dataset' is the whole set of points to search from
%'K' is the number of neighbors
function [indexes dists] = findneighbors (refpoint,dataset,K)
    if size(refpoint) ~= size(dataset(1,:))
        error('Number of coordinates of refpoint must be equal to dataset');
    end
    if K < 1
        error('Number of neighbors qty must be greater or equal to 1');
    end
    if K > size(dataset,1)
        error('Dataset size must be greater than qty');
    end
    
    dists     = zeros(K,1);
    indexes = zeros(K,1);
    dists(:) = intmax; %initializes all distances with big numbers
        
    datasetSize = size(dataset,1);    
    for i=1:datasetSize
        if dataset(i,:) ~= refpoint %excludes refpoint from neighbors list (in case refpoint is inside dataset)
            D = sqrt( sum( (dataset(i,:) - refpoint) .^ 2) ); %euclidean distance
            j=1;
            while (j <= K) && (D>dists(j))
                j = j+1;
            end
            if j <= K %This point is between the closest qty points
                for c=K:-1:(j+1) %opens up a space between the qty closest pairs (discard farthest one with index=qty)
                    dists(c) = dists(c-1); %bring everyone down one position to insert new closest point
                    indexes(c) = indexes(c-1);
                end
                dists(j) = D;
                indexes(j) = i;
            end
        end
    end
end

