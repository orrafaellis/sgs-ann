import sgems

sgems.execute('DeleteObjects SIM')
sgems.execute('DeleteObjects finished')
sgems.execute('NewCartesianGrid  SIM::50::30::1::1::1::1::0::0::0')
sgems.execute('LoadObjectFromFile  obs.sgems::All')
sgems.execute('DeleteObjects finished')
sgems.execute('RunGeostatAlgorithm  dssim::/GeostatParamUtils/XML::<parameters>    <algorithm name="dssim" />    <Grid_Name value="SIM" />    <Property_Name value="DSSIM" />    <Nb_Realizations value="9" />    <Seed value="14071789" />    <Kriging_Type value="Simple Kriging (SK)" />    <Trend value="0 0 0 0 0 0 0 0 0" />    <Local_Mean_Property value="" />    <SK_mean value="0" />    <Hard_Data grid="OBS" property="DATA" />    <Assign_Hard_Data value="1" />    <Max_Conditioning_Data value="12" />    <Search_Ellipsoid value="80 80 80 0 0 0" />    <cdf_type value="LogNormal" />    <LN_mean value="1" />    <LN_variance value="1" />    <U_min value="0" />    <U_max value="1" />    <nonParamCdf ref_on_file="0" ref_on_grid="1" break_ties="0" filename="" grid="" property="">        <LTI_type function="Power" extreme="0" omega="3" />        <UTI_type function="Power" extreme="0" omega="0.333" />    </nonParamCdf>    <is_local_correction value="1" />    <Variogram nugget="0.001" structures_count="1">        <structure_1 contribution="0.999" type="Gaussian">            <ranges max="30" medium="30" min="30" />            <angles x="0" y="0" z="0" />        </structure_1>    </Variogram></parameters>')


sgems.execute('SaveGeostatGrid  SIM::DSSIM.out::gslib::0::DSSIM__real0::DSSIM__real1::DSSIM__real2::DSSIM__real3::DSSIM__real4::DSSIM__real5::DSSIM__real6::DSSIM__real7::DSSIM__real8')
sgems.execute('SaveGeostatGrid  SIM::DSSIM.sgems::s-gems::0::DSSIM__real0::DSSIM__real1::DSSIM__real2::DSSIM__real3::DSSIM__real4::DSSIM__real5::DSSIM__real6::DSSIM__real7::DSSIM__real8')


sgems.execute('NewCartesianGrid  finished::1::1::1::1.0::1.0::1.0::0::0::0')
data=[]
data.append(1)
sgems.set_property('finished','dummy',data)
sgems.execute('SaveGeostatGrid  finished::finished::gslib::0::dummy')
