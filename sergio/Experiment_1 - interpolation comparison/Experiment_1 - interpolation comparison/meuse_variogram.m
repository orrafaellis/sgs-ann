%Spherical variogram with anisotropy angle and fraction
function [VAR] = meuse_variogram(estimate_location,known_point_location)
    sill=0.59;
    effective_range=1200;
    angle=(45/180)*pi;
    fraction=0.4;
    
    aMax = effective_range/sqrt(3);

    delta = (estimate_location-known_point_location);
    fi = atan(delta(1,2) / delta(1,1));
    h = sqrt(sum((estimate_location-known_point_location).^2));
    
    
    new_range = (aMax*fraction) / sqrt((fraction^2) * cos(fi-angle)^2 + sin(fi-angle)^2);
    
    if(h<=new_range)
        VAR = sill * ( 1.5*(h/new_range) - 0.5*(h/new_range)^3 );
    else
        VAR = sill;
    end
end