%Using SGeMS sgsim algorithm with  Meuse River Dataset

%main parameters
variogram='0.05 Nug(0) + 0.59 Sph(1200,45,.4)'; %Variogram model for kriging
realizations = 1;
trainingSetSize = 155;
%grid definition
nx = 55;
ny = 77;
dx=50;
dy=50;
x0=178605;
y0=329714;
datasetPath='datasets/meuse/meuse.txt';
delimiter=',';

run_sgs=1;
run_siigmn_estimation=1;
run_siigmn_simulation=1;
plot_variograms=0;
load_krig_from_file=0;
krig_file='krig77x55_walker_sim.mat';

%Builds grid similar to the one used in sgems for use in kriging and siigmn
grid = siigmn.build_grid(nx,ny,dx,dy,x0,y0);

fprintf('Loading dataset: %s ...\n',datasetPath);
ds = dataset('File',datasetPath,'Delimiter',delimiter);
%Prints dataset summary
%summary(ds)
%meuse river dataset
%x: [155x1 double] -> max(ds.x)-min(ds.x)=2785
%        min          1st quartile    median        3rd quartile    max       
%      1.786e+05    1.7935e+05      1.7999e+05    1.8063e+05      1.8139e+05
%y: [155x1 double] -> max(ds.y)-min(ds.y)=3897
%    min           1st quartile    median        3rd quartile    max 
%    3.2971e+05    3.3076e+05      3.3163e+05    3.3247e+05      3.3361e+05
%zinc: [155x1 double] -> max(ds.zinc)-min(ds.zinc)=1726
%    min    1st quartile    median    3rd quartile    max 
%    113    198             326       675.25          1839
    
x = ds.x;
y = ds.y;
%z = ds.v; %chooses variable for interpolation
z = ds.zinc;

range_x=max(ds.x)-min(ds.x)
range_y=max(ds.y)-min(ds.y)
range_z=max(z)-min(z)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%SGeMS SGS - Sequential Gaussian Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

S = sgems_get_par('sgsim'); %loads parameter file for sgsim
%grid definition (simulation set):
S.dim.nx=nx;
S.dim.ny=ny;
S.dim.nz=1;
% grid cell size
S.dim.dx=dx;
S.dim.dy=dy;
S.dim.dz=1;
% grid origin
S.dim.x0=x0;
S.dim.y0=y0;
S.dim.z0=0;

%Sets the variogram model
S.XML.parameters.Variogram = sgems_variogram_xml(variogram);

%sets number of realizations
S.XML.parameters.Nb_Realizations.value = realizations;

% Builds simulationSet and trainingSet
% Randomly removes 'simulationSetSize' samples from the dataset to use for simulation
fprintf('Constructing Training Sets. \nDataset size = %d  | Training Set size = %d \n Press any key to continue...\n',numel(x),trainingSetSize);
%trainingSet = dataset({x,'x'},{y,'y'},{z,'z'}); %Builds training set
trainingSetIndex = randsample(numel(x),trainingSetSize);
trainingSet = dataset({x(trainingSetIndex),'x'},{y(trainingSetIndex),'y'},{z(trainingSetIndex),'z'}); %builds training set

fprintf('\nTraining set summary: \n');
summary(trainingSet);
fprintf('\nTraining set successfuly constructed! Summary presented above...');

%sets conditioning data
d_obs = [trainingSet.x trainingSet.y zeros(numel(trainingSet.x),1) trainingSet.z];
sgems_write_pointset('obs.sgems',d_obs);
S.f_obs='obs.sgems';

if run_sgs 
fprintf('\nRuning SGeMS SGS...\n');
S=sgems_grid(S)


if realizations < 12
    plots = realizations;
else
    plots = 12;
end
figure
for i=1:plots; %consertar isso para exibir corretamente o eixo y.
  subplot(3,4,i);
  imagesc(S.x,S.y,S.D(:,:,1,i));
  set(gca,'YDir','normal'); %imagesc plots y axis inverted if not use this option
end

%mean_sgs = mean(S.data,2);
mean_sgs = zeros(ny,nx);
for i=1:ny
    for j=1:nx
        mean_sgs(i,j) = mean(S.D(j,i,1,:));
    end
end
%mean_sgs = reshape(mean_sgs,nx,ny);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Kriging Estimation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('\nProcessing kriging...\n');
if load_krig_from_file==1
    load(krig_file);
else
    [est_krig,var_krig] = krig([trainingSet.x trainingSet.y],trainingSet.z,grid,variogram);
    save('krig140x195_walker_sim.mat','est_krig','var_krig');
end

est_krig = reshape(est_krig,ny,nx);

grid_x = x0:dx:(x0+dx*(nx-1));
grid_y = y0:dy:(y0+dy*(ny-1));

figure
%Plots dataset bubble plot
subplot(1,5,1)
fprintf('Plotting bubble plot for Meuse River dataset...\n');
scatter(trainingSet.x,trainingSet.y,trainingSet.z);
title('Meuse River dataset');

%Plots kriging estimation
subplot(1,5,2);
imagesc(grid_x,grid_y,est_krig);
title('Kriging');
set(gca,'YDir','normal'); %imagesc plots y axis inverted if not use this option

%Plots mean of SGS realizations
if run_sgs
    subplot(1,5,3);
    imagesc(S.x,S.y,mean_sgs);
    set(gca,'YDir','normal'); %imagesc plots y axis inverted if not use this option
    title('Mean of SGS realizations');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%SIIGMN Estimation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if run_siigmn_estimation
    fprintf('\nProcessing SIIGMN estimation...');
    options = [];
    options.Display = 'off';

    %Options specific for SIIGMN
    trend = mean(trainingSet.z);
    data = [trainingSet.x trainingSet.y trainingSet.z];
    delta = 0.1;
    range = max(data)-min(data);    
    model = siigmn('range', range,'CovType', 'full', 'tau', 0.05, 'delta', delta, 'vmin', 0, 'spmin', 0, 'covAngle',-45, 'Options', options);

    model = model.train(data);%trains the model
    est_siigmn= model.recall(grid,trend);

    est_siigmn = reshape(est_siigmn,ny,nx);

    %Plots siigmn estimation
    subplot(1,5,4);
    imagesc(grid_x,grid_y,est_siigmn);
    set(gca,'YDir','normal'); %imagesc plots y axis inverted if not use this option
    title('SIIGMN estimation');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%SIIGMN simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if run_siigmn_simulation
    fprintf('\nProcessing SIIGMN simulation...');
    options = [];
    options.Display = 'off';
    %trend = mean(trainingSet.z);

    [z_nscore, o_nscore] = nscore(trainingSet.z); %normal score transform (only necessary on simulation but I included here for testing)
    data = [trainingSet.x trainingSet.y z_nscore];
    %data
    %data = [trainingSet.x trainingSet.y trainingSet.z];
    delta = 0.1;
    range = max(data)-min(data);

    siigmn_model = siigmn('range', range,'CovType', 'full', 'tau', 0.05, 'delta', delta, 'vmin', 0, 'spmin', 0, 'covAngle',14, 'Options', options);

    siigmn_model = siigmn_model.train(data);%trains the model
    siigmn_sim = siigmn_model.simulate(grid,realizations);

    for ss=1:size(siigmn_sim,2) %inverse normal score transform
        siigmn_sim(:,ss) = inscore(siigmn_sim(:,ss),o_nscore);
    end

    siigmn_sim_mean = mean(siigmn_sim,2);
    %igmn_sim_var = var(igmn_sim')';
    
    %plots means of SIIGMN realizations
    siigmn_sim_mean = reshape(siigmn_sim_mean,ny,nx);
    subplot(1,5,5);
    imagesc(grid_x,grid_y,siigmn_sim_mean);
    set(gca,'YDir','normal'); %imagesc plots y axis inverted if not use this option
    title('Mean of SIIGMN realizations');
    
    %plots 16 (or less) realizations of siigmn simulation
    if realizations < 16
        plots = realizations;
    else
        plots = 16;
    end
    figure
    for i=1:plots;
      subplot(4,4,i);
      realization_i = reshape(siigmn_sim(:,i),ny,nx);
      imagesc(grid_x,grid_y,realization_i);
      set(gca,'YDir','normal'); %imagesc plots y axis inverted if not use this option
    end
end


%**************************************************************************
%PLOTS HISTOGRAMS
%**************************************************************************
figure
subplot(1,4,1);
hist(z);
title('Original data');

subplot(1,4,2);
hist(est_krig);
title('Kriging');

subplot(1,4,3);
hist(est_siigmn);
title('IGMN');

subplot(1,4,4);
hist(siigmn_sim_mean);
title('Mean SIIGMN realizations');


%**************************************************************************
%PLOTS VARIOGRAMS
%**************************************************************************
if plot_variograms
    figure
    %theoretical variogram
    [sv,d] = semivar_synth(variogram,[0:dx:2500]);
    sv = sv*20e+04; %scala o variograma para plotar. Não sei porque tive que fazer isso na mão.
    plot(d,sv,'-r');

    %experimental variograms
    hold on %training data exp variogram
    [gamma,h,angle_center]=semivar_exp([x y],z,dx,[30 60]);
    plot(h,gamma,'+r');

    hold on %siigmn mean simulation variogram
    [gamma,h,angle_center]=semivar_exp(grid,mean(siigmn_sim,2),dx,[30 60]);
    plot(h,gamma,'+g');

    hold on %siigmn mean SGS variogram
    [gamma,h,angle_center]=semivar_exp(grid,mean(S.data,2),dx,[30 60]);
    plot(h,gamma,'+b');

    legend('theoretical','dataset','siigmn estimation','siigmn simulation','SGS');
end



