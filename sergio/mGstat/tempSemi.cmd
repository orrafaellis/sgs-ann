data(tempSemi): 'tempSemi.eas', x=1, y=2, z=3, v=4;
method: semivariogram;
variogram(tempSemi): 'tempSemi.variogram';
set alpha =  83.5000;
set tol_hor = 15;
set width =   0.4000;
set cutoff = 6;
set format = '%12.8g';
