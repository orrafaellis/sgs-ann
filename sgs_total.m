    clear;close all;
    well = load('well.mat');
    well = well.well;
    columns = well.curve_info(:,1);

%defini��o do grid
    depth = well.curves(2500:5000,find(ismember(columns,'DEPTH')));
    output = well.curves(2500:5000,find(ismember(columns,'VP')));
    output0 = output;
    par1 = output - min(output);
    par2 = par1./max(par1);
    output = par2;
    
    
    out_simulation = -999*ones(size(depth,1),2);
    out_simulation(:,1)= depth;
    num_cells = length(depth);

%plot e defini��o de ponto condicionante:
    figure('units','normalized','outerposition',[0 0 1 1]);
    subplot(2, 3, 1);
    plot(depth(output~=-999),output(output~=-999),'color','black');
    hold on;
    cp = 1; 
    
    
    cp_log = zeros(size(depth));
    
    cond_method = 3;
    for a = 1:1
        switch cond_method
            case 1
            %selecionar pontos condicionantes:
                if cp
                   for a=1:1
                        pause(0.001);
                        rect_info = getrect();
                        interval = 1;
                        selected_interval = [rect_info(1) rect_info(1)+rect_info(3)];
                        start_cp = find(abs(depth-selected_interval(1))==min(abs(depth-selected_interval(1))));
                        end_cp = find(abs(depth-selected_interval(2))==min(abs(depth-selected_interval(2))));

                        start_cp=49;
                        end_cp=452;


                        cp_log(start_cp:end_cp) = 1;
                    end
                end

            case 2
            %gerar intervalo de condicionais
                cp_log((output~=-999)&(~isnan(output))) = 1;
            case 3
            %gerar condicionais rand�micos:
                pause(0.001);
                rect_info = getrect();
                interval = 1;
                selected_interval = [rect_info(1) rect_info(1)+rect_info(3)];
                start_cp = find(abs(depth-selected_interval(1))==min(abs(depth-selected_interval(1))));
                end_cp = find(abs(depth-selected_interval(2))==min(abs(depth-selected_interval(2))));
    %             start_cp=12;
    %             end_cp=844;
                indices = (start_cp:end_cp)';
    %             indices = indices(~isnan(output)&output~=-999);
    %             indices = indices(randi(length(indices),1,420));
                cp_log(indices) = 1;
        end
    end
    
    cp_log = logical(cp_log);
%     [output(cp_log) params_nscore] = nscore(output(cp_log));
    out_simulation(cp_log,2) = output(cp_log);
    plot(depth(cp_log),output(cp_log),'*','color','red');
    plot(depth(output~=-999),output(output~=-999),'color','black');
    
            
    %plotar variograma e histograma dos dados
    %   x = output((output~=-999)&~isnan(output));
        x = output(cp_log);
        x= x - mean(x);
        covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
        covariogram = covariogram / max(covariogram);
        subplot(2, 3, 2);
        plot(covariogram);
        title(['Variogram range: ',num2str(get_cpnoise_range(output(cp_log),1))]);
        subplot(2, 3, 3);
        histogram(output(cp_log));
    %

 %treinamento da rede
    training_data = [depth,output];
    training_data = training_data(cp_log,:);
    %configura��o
    range = max(training_data) - min(training_data);
%     delta = 0.45; tau = 0.1; vmin = 2; spmin = 3; covtype = 'full';
    options = [];
    options.Display = 'final';
    %tau controlou o n�vel de ruido:inversamente proporcional
    model = igmn('CovType', 'full', 'range', range, 'tau',0.1, 'delta', 0.1, 'vmin', 40, 'spmin',...
        3, 'Options', options,'corr_coef',0,'range_trend',100,'target_means',0.7);
%     model = igmn('range',range,'delta',delta,'tau',tau,'vmin',vmin,'spmin',spmin,'covtype', covtype);
    model = model.train(training_data);
    subplot(2, 3, 1);
    %plotar as gaussianas
    for cluster_id = 1:model.nc
        mean_i = model.means(cluster_id,:);
        cov_i = model.covs(:,:,cluster_id);
        plot(mean_i(1),mean_i(2),'v','color',[201, 34, 227]/255);
        plot_gaussian_ellipsoid(mean_i, cov_i);
    end
    

%simula��o
    available_points_index = logical(ones(length(depth),1));
    %figure;
%     for sim_id = 1:sum(double(~cp_log))
    means =[];
    num_points_sim = length(depth);
    subplot(2, 3, 3);
    for sim_id = 1:num_points_sim
        
        waitbar(sim_id/num_points_sim);
        available_points = (1:length(depth))';
        available_points = available_points(available_points_index);
        actual_p_index = randi(length(available_points));
        actual_p = available_points(actual_p_index);
        available_points_index(actual_p) = false;
        
        %estimar media e cov
        dist_params = model.recall4(depth(actual_p));
        medias = dist_params{1};
        log_point = mvnrnd(medias,dist_params{2},1);
                gaussian = dist_params{3};
                gaussian_mean = gaussian{1};
                subplot(2, 3, 4);cla;

                 for cluster_id = 1:model.nc
                        mean_i = model.means(cluster_id,:);
                        cov_i = model.covs(:,:,cluster_id);
                        plot(mean_i(1),mean_i(2),'v','color',[201, 34, 227]/255);
                        plot_gaussian_ellipsoid(mean_i, cov_i);
                 end
                trend_position = dist_params{3}{1};
                plot(trend_position(1),trend_position(2),'p','color',[100, 34, 100]/255);
                dist_params{2}
                
                means = [means;depth(actual_p),medias];
        
                %fft:
                
                
                plot(depth(actual_p),medias,'o','color','blue');hold on;
                plot(depth(actual_p),log_point,'+','color','green');hold on;
                
        subplot(2, 3, 5);
        %catar ponto:        
        out_simulation(actual_p,2) = log_point;
%         plot(depth(actual_p),dist_params(1),'o','color','blue');hold on;
        plot(depth(actual_p),log_point,'+','color','black');hold on;
        %adicionar ponto � rede:
        model = model.train(out_simulation(actual_p,:));
        
        
        
        subplot(2, 3, 4);cla;
        for cluster_id = 1:model.nc
                mean_i = model.means(cluster_id,:);
                cov_i = model.covs(:,:,cluster_id);
                plot(mean_i(1),mean_i(2),'v','color',[201, 34, 227]/255);
                plot_gaussian_ellipsoid(mean_i, cov_i);
        end
          plot(means(:,1),means(:,2),'*','color','blue');
%           indexes = (~isnan(out_simulation(:,2)))&(out_simulation(:,2)~=-999);
        plot(out_simulation(~available_points_index,1),out_simulation(~available_points_index,2),'x','color','black');
%         plot(depth(cp_log),output(cp_log),'o','color','red');hold on;
       
        
        
        hold on;
    end
    
    
%     out_simulation(:,2) = inscore(out_simulation(:,2),params_nscore);
    means(:,2) = inscore(means(:,2),params_nscore);
    
    
    
    subplot(2, 3, 4);cla
    plot(means(:,1),means(:,2),'x','color','red'); 
    plot(out_simulation(out_simulation(:,2)~=-999,1),out_simulation(out_simulation(:,2)~=-999,2),'color','black');hold on;
% plot(depth(cp_log),output(cp_log),'x','color','green');
hold on
%plotar as m�dias das gaussianas
for cluster_id = 1:model.nc
   mean_i = model.means(cluster_id,:);
   plot(mean_i(1),mean_i(2),'o','color',[179, 30, 201]/255);
end

%variograma da simula��o
x = out_simulation(:,2);
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram = covariogram / max(covariogram);
subplot(2, 3, 5);cla;
plot(covariogram);hold on
[~,idx]=sort(means(:,1));

x = means(idx,2);
x= x - mean(x);
covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
covariogram = covariogram / max(covariogram);
subplot(2, 3, 5);
plot(covariogram);hold off

subplot(2, 3, 6);cla
histogram(out_simulation(:,2));
