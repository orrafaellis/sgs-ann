function out_array = create_pattern(i, size, mode)
%input: i = [1 2 3 4]
%mode 1: out = [1 2 3 4 3 2 1 2 3 4]
%mode 2: out = [4 3 2 1 2 3 4 3 2 1]
switch mode
    case 1
        ii = [i;flip(i)];
        iii = (repmat(ii,size,1,1))';
        iiii = iii(:);
        iiii(diff(iiii)==0) = [];
        out_array = iiii(1:size);
    case 2
        i = flip(i);
        ii = [i;flip(i)];
        iii = (repmat(ii,size,1,1))';
        iiii = iii(:);
        iiii(diff(iiii)==0) = [];
        out_array = iiii(1:size);
    case 3
        iii = (repmat(i,size,1,1))';
        iiii = iii(:);
        iiii(diff(iiii)==0) = [];
        out_array = iiii(1:size);
end