function [out_simulation] = sgs_play_testes(conditioning_log,original_logs,depth_log,...
tau,delta,vmin,spmin,trend_range,means,num_realiz,nome_arquivo_treinamento,com_silicato)
foi = 0;
while ~foi
try
%grade:
out_simulation = -999*ones(size(depth_log,1),1+size(original_logs,2));
out_simulation(:,1)= depth_log;
% out_simulation(conditioning_log,2:end) = original_logs(conditioning_log,:);
%ajuste dos dados:
training_data = [depth_log(conditioning_log,:), original_logs(conditioning_log,:)];
out_simulation(conditioning_log,:) = training_data;
if ~com_silicato
    training_data = training_data(4500:end,:);
end

output = training_data(:,2:end);
par2 = (output - min(output))./(ones(size(output)).*(max(output)-min(output)));
training_data(:,2:end) = par2;
% %nscore
%     backparams = {};
%     for log_id = 1:(size(training_data,2)-1)
%         [training_data(:,log_id+1) backs] = nscore(training_data(:,log_id+1));
%         backparams = [backparams,backs];
%     end
% %

%configuração
range = max(training_data) - min(training_data);
options = [];
options.Display = 'final';
modelo = 1;

    switch modelo
        case 1
            model = igmn('CovType', 'full', 'range', range, 'tau',tau, 'delta', delta, 'vmin', vmin, 'spmin',...
                spmin, 'Options', options,'corr_coef',0,'range_trend',trend_range,'target_means',means);
        case 2
            model = siigmn('range', range,'CovType', 'full', 'tau', tau, 'delta', delta, 'vmin', 0, 'spmin', 0, 'Options', options,'trend_range',trend_range);
    end


save(nome_arquivo_treinamento,'training_data');



model = model.train(training_data);
figure;
plot(training_data(:,2),training_data(:,3),'x');
    %simulação
    
region_to_sim = depth_log;%(logical(conditioning_log));
points_to_sim = length(region_to_sim);

available_points = (1:points_to_sim)';
simulated_points = zeros(points_to_sim,1);

wbw = waitbar(0,{'';'Simulating Points for facies: ';''},'Name', 'Simulation Progress for facies', ...
'CreateCancelBtn', 'setappdata(gcbf,''canceling'',1)');

% 
% figure(3); %v
% figure(4);

available_points_index = logical(ones(length(depth_log),1));
for point_id = 1:points_to_sim
   
    waitbar(point_id/points_to_sim,wbw);
        available_points = (1:length(depth_log))';
        available_points = available_points(available_points_index);
        actual_p_index = randi(length(available_points));
        actual_p = available_points(actual_p_index);
        available_points_index(actual_p) = false;
        
    
    
    %avaliar_ponto
    estimation_parameters = model.recall3(depth_log(actual_p));
    
%     triu(estimation_parameters{2}) = tril(estimation_parameters{2})'
    log_point = mvnrnd(estimation_parameters{1},estimation_parameters{2},1); 

    out_simulation(actual_p,2:end) = log_point;
%     figure(3);
%     plot(log_point(1),log_point(2),'x');hold on; %v
%     figure(4)
%      plot(depth_log(actual_p),log_point(1),'x');hold on;
%     pause(0.0001);


     model = model.train([depth_log(actual_p),log_point]);
end
foi = 1;
catch
delete(wbw)  
foi = 0;
end
end
delete(wbw)
%inscore
    
%     for log_id = 1:size(out_simulation,2)-1
%         [out_simulation(:,log_id+1)] = inscore(out_simulation(:,log_id+1),backparams{log_id});
%     end
%     
%

% figure;plot(out_simulation(out_simulation(:,2)~=-999,1),out_simulation(out_simulation(:,2)~=-999,2))
