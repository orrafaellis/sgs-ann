function corresp_indexes = mps(matrix,pattern)
    M = zeros(size(matrix,1) - length(pattern),length(pattern));
    A = zeros(size(matrix,2)*2,1);
    for i =1:1:size(matrix,2) - length(pattern)
        index = i: i + length(pattern)-1;
        M(i,:) = matrix(index);
    end

    index = knnsearch(M,pattern);
    corresp_indexes = index: index + length(pattern)-1;
    
    
    
    
    

end

