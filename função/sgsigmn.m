function [out] = sgsigmn(conditioning_log,original_logs,depth_log,...
tau,delta,vmin,spmin,trend_range,means,num_realiz)
%grade:
out_simulation = -999*ones(size(depth_log,1),1+size(original_logs,2));
out_simulation(:,1)= depth_log;
out_simulation(conditioning_log,2:end) = original_logs(conditioning_log,:);
%ajuste dos dados:
training_data = [depth_log(conditioning_log,:), original_logs(conditioning_log,:)];
%configura��o
range = max(training_data) - min(training_data);
options = [];
options.Display = 'final';
out = {};
modelo = 1;
for realization_id = 1:num_realiz
    
    switch modelo
        case 1
            model = igmn('CovType', 'full', 'range', range, 'tau',tau, 'delta', delta, 'vmin', vmin, 'spmin',...
                spmin, 'Options', options,'corr_coef',0,'range_trend',trend_range,'target_means',means);
        case 2
            model = siigmn('range', range,'CovType', 'full', 'tau', tau, 'delta', delta, 'vmin', 0, 'spmin', 0, 'Options', options,'trend_range',trend_range);
    end
%      [training_data(:,2) inscore_pars] = nscore(training_data(:,2));
    model = model.train(training_data);

    %simula��o
    available_points_index = logical(ones(length(conditioning_log),1));
    num_points_sim = size(conditioning_log,1);

    for sim_id = 1:num_points_sim
        available_points = (1:length(conditioning_log))';
        available_points = available_points(available_points_index);
        actual_p_index = randi(length(available_points));
        actual_p = available_points(actual_p_index);
        available_points_index(actual_p) = false;

        %estimar media e cov
        switch modelo
            case 1
                dist_params = model.recall4(depth_log(actual_p));
                medias = dist_params{1};
                log_point = mvnrnd(medias,dist_params{2},1);
            case 2
                [medias vars] = model.recall_v(depth_log(actual_p));
                log_point = mvnrnd(medias,vars,1);
        end
        out_simulation(actual_p,2:end) = log_point;
        %adicionar ponto � rede:
        model = model.train(out_simulation(actual_p,:));
    end
%     out_simulation(:,2) = inscore(out_simulation(:,2),inscore_pars);
    out = [out,out_simulation];
end
end

