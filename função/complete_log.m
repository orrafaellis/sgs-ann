%carregamento do po�o:
clear;close all;
well = load('well.mat');
well = well.well;
columns = well.curve_info(:,1);

% depth = (linspace(0,6*pi,10000))';
% output = sin(depth);
% depth = well.curves(indexes,find(ismember(columns,'DEPTH')));

% range_noise = 6;
properties = {'VP'};
depth = well.curves(2500:7000,find(ismember(columns,'DEPTH')));
output = well.curves(2500:7000,find(ismember(columns,'VP')));
    
% output = fftma_l3c(length(depth),1,range_noise,range_noise,0);
% output = load('output_entrada_fftma.mat');
% output = output.output;

% output = output(indexes,:);

%entradas da fun��o:

% conditioning_log; 
% original_logs;
% properties_index;
% tau;
% delta;
% vmin;
% spmin;
% trend_range;
% means; 
% num_realiz;

%% normaliza��o:    
output0 = output;
par1 = output - min(output);
par2 = par1./max(par1);
original_logs = par2;
%% %plot
figure1=figure('units','normalized','outerposition',[0 0 1 1]);
subplot(2, 3, 1);
indexes = ~sum(isnan(original_logs)&original_logs==-999,2);
for plot_id = 1:size(original_logs,2)
    plot(depth(indexes),original_logs(indexes,plot_id));
    hold on;
end
%% defini��o de pontos condicioantes:
conditioning_log = zeros(size(depth));
pause(0.000000000001);
rect_info = getrect();
selected_interval = [rect_info(1) rect_info(1)+rect_info(3)];
start_cp = find(abs(depth-selected_interval(1))==min(abs(depth-selected_interval(1))));
end_cp = find(abs(depth-selected_interval(2))==min(abs(depth-selected_interval(2))));
% start_cp=1;
% end_cp=2000;
indices = (start_cp:end_cp)';
%indices = indices(~isnan(output)&output~=-999);
%indices = indices(randi(length(indices),1,450));
conditioning_log(indices) = 1;
conditioning_log = logical(conditioning_log);
%% plotar dos pontos condicionantes:
plot(depth(conditioning_log),conditioning_log(conditioning_log),'*','color','red');
%% plot dos variogramas e histogramas
for plot_id = 1:size(original_logs,2)
        x = original_logs(conditioning_log,plot_id);
        x= x - mean(x);
        covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
        covariogram = covariogram / max(covariogram);
        subplot(6, 6, 2+plot_id);
        plot(covariogram);
        title(['Variogram for ',properties{plot_id}, ', R: ',num2str(get_cpnoise_range(original_logs(conditioning_log,plot_id),1))]);
        subplot(6, 6, 8+plot_id);
        histogram(original_logs(conditioning_log,plot_id));
        title(['Histogram for ',properties{plot_id}]);
end
pause(0.000000000001);
%% simula��o
num_sims = 1;
figures = {};
range_differences = [];
for simu_id=1:num_sims
    close all;
    figures = [figures,figure('units','normalized','outerposition',[0 0 1 1])];
    %plot dos logs:
    subplot(2, 3, 1);
    for plot_id = 1:size(original_logs,2)
        plot(depth(indexes),original_logs(indexes,plot_id));
        hold on;
    end
    %plot dos pontos condicionantes:
    plot(depth(conditioning_log),conditioning_log(conditioning_log),'*','color','red');
    %plot dos variogramas e histogramas:
    ranges_logs_of_inputs = [];
    for plot_id = 1:size(original_logs,2)
            x = original_logs(conditioning_log,plot_id);
            x= x - mean(x);
            covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
            covariogram = covariogram / max(covariogram);
            subplot(6, 6, 2+plot_id);
            plot(covariogram);
            ranges_logs_of_inputs = [ranges_logs_of_inputs,get_cpnoise_range(original_logs(conditioning_log,plot_id),1)];
            title(['Variogram for ',properties{plot_id}, ', R: ',num2str(ranges_logs_of_inputs(plot_id))]);
            subplot(6, 6, 8+plot_id);
            histogram(original_logs(conditioning_log,plot_id));
            title(['Histogram for ',properties{plot_id}]);
    end
    pause(0.000000000001);
    
    
    
    
    tau=0.05; %0.001 at� 0.5
    delta=0.01; %0.001 at� 0.5   
    vmin=200; %1 at� 50
    spmin=1; %1 at� 20
    trend_range = 3;
%     tau   = (0.5-0.001).*rand + 0.001;
%     delta = (0.5-0.001).*rand + 0.001;
%     vmin = (100-1).*rand + 1;
%     spmin = (20-1).*rand + 1;
%     trend_range =(1000-15).*rand + 10;

annotation(figures(simu_id),'textbox',...
        [0.3963125 0.0498504486540379 0.2411875 0.168494516450647],...
        'String',{['realization: ',num2str(simu_id)],['tau: ',num2str(tau)],['delta: ',num2str(delta)],...
        ['vmin: ',num2str(vmin)],['spmin: ',num2str(spmin)],['trend range: ',num2str(trend_range)]}, 'FitBoxToText','off');
    pause(0.000000000001);
%      try
        means=[]; 
        num_realiz=1;
        saidas = sgs_play(conditioning_log,original_logs,depth,...
        tau,delta,vmin,spmin,trend_range,means,num_realiz);
        out_simulation = saidas{1};
        subplot(2, 3, 6);cla
        if size(out_simulation,2)>2
            plot(out_simulation(:,2),out_simulation(:,3),'x');
        end
        ranges_logs_of_output = [];
        for plot_id = 1:size(output,2)
            x = out_simulation(:,plot_id+1);
            x= x - mean(x);
            covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
            covariogram = covariogram / max(covariogram);
            subplot(6, 6, 20+plot_id);
            plot(covariogram);
            ranges_logs_of_output = [ranges_logs_of_output,get_cpnoise_range(out_simulation(:,1+plot_id),1)];
            title(['Variogram for ',properties{plot_id}, ', R: ',num2str(ranges_logs_of_output(plot_id))]);
            subplot(6, 6, 26+plot_id);
            histogram(out_simulation(:,plot_id+1));
            title(['Histogram for ',properties{plot_id}]);
        end
        
        subplot(2, 3, 4);cla
        for plot_id = 1:size(output,2)
            index = (~isnan(out_simulation(:,1+plot_id)))&(out_simulation(:,1+plot_id)~=-999);
            plot(out_simulation(index,1),out_simulation(index,1+plot_id));
            hold on;
        end
        legend(properties)
        
        variogram_difference_mean = mean(abs(ranges_logs_of_output-ranges_logs_of_inputs));
        
    
%          saveas(figures(simu_id),['fun��o/saidas_com_nscore/realiza��o_',num2str(simu_id),'.jpg']);
%          variogram_difference_mean = mean(abs(ranges_logs_of_output-ranges_logs_of_inputs));
%         range_differences = [range_differences;simu_id,variogram_difference_mean,tau,delta,vmin,spmin,1];
%      catch
%          saveas(figures(simu_id),['fun��o/saidas_com_nscore/fails/realiza��o_fail',num2str(simu_id),'.jpg']);
%          range_differences = [range_differences;simu_id,-99,tau,delta,vmin,spmin,0];
%      end
end
% save('fun��o/saidas_com_nscore/range_differences.mat','range_differences');