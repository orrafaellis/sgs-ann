
simulou = 0;

while (~simulou)
    
    try
        clear;close all;
        % well = load('wells/well_lucas1.mat');
        com_silicato = 0;
        
        propertie = {'VP','RHOB'};
        nome_arquivo_treinamento = 'saidastestes/vp_rho_treinamento.mat';
        nome_arquivo_saida = 'saidastestes/vp_rho.mat';
        well = read_las2_file('Well_1.las');

        columns = well.curve_info(:,1);

        properties_index = find(ismember(columns,propertie));
        output = well.curves(:,properties_index);
        

        depth = well.curves(:,find(ismember(columns,'DEPTH')));


        original_logs = output;
        %% defini��o de pontos condicioantes:
        conditioning_log = zeros(size(depth));

        indices = ~logical(sum(isnan(output)|output==-999,2));
        conditioning_log(indices) = 1;

        conditioning_log = logical(conditioning_log);
        %% simula��o
        num_sims = 1;
        figures = {};
        range_differences = [];
        for simu_id=1:num_sims
%             close all;
%             figures = [figures,figure('units','normalized','outerposition',[0 0 1 1])];
            %plot dos logs:


            tau=0.1; %0.001 at� 0.5
            delta=0.08; %0.001 at� 0.5   
            vmin=200; %1 at� 50
            spmin=3; %1 at� 20
            trend_range = 100;
        
                means=[]; 
                num_realiz=1;
                saidas = sgs_play_testes(conditioning_log,original_logs,depth,...
                tau,delta,vmin,spmin,trend_range,means,num_realiz,nome_arquivo_treinamento,com_silicato);
                save(nome_arquivo_saida,'saidas');
                simulou = 1;
        end
        
    catch
        simulou = 0;
        close all;
    end
end

