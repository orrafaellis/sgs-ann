%carregamento do po�o:


simulou = 0;

while (~simulou)
    
    try
        clear;close all;
        % well = load('wells/well_lucas1.mat');
        com_silicato = 0;
        
        propertie = {'VP','VS','RHOB'};
        nome_arquivo_treinamento = 'saidaslucas2/vp_vs_rho_treinamento.mat';
        nome_arquivo_saida = 'saidaslucas2/vp_vs_rho.mat';
        well = read_las2_file('Well_1.las');

        columns = well.curve_info(:,1);

        properties_index = find(ismember(columns,propertie));
        output = well.curves(:,properties_index);
        

        depth = well.curves(:,find(ismember(columns,'DEPTH')));


        original_logs = output;
        %% %plot
        figure1=figure('units','normalized','outerposition',[0 0 1 1]);
        subplot(2, 3, 1);
        indexes = ~sum(isnan(original_logs)&original_logs==-999,2);
        for plot_id = 1:size(original_logs,2)
            plot(depth(indexes),original_logs(indexes,plot_id));
            hold on;
        end
        %% defini��o de pontos condicioantes:
        conditioning_log = zeros(size(depth));

        indices = ~logical(sum(isnan(output)|output==-999,2));
        conditioning_log(indices) = 1;

        conditioning_log = logical(conditioning_log);
        %% plotar dos pontos condicionantes:
        plot(depth(conditioning_log),conditioning_log(conditioning_log),'*','color','red');

        %% simula��o
        num_sims = 1;
        figures = {};
        range_differences = [];
        for simu_id=1:num_sims
            close all;
            figures = [figures,figure('units','normalized','outerposition',[0 0 1 1])];
            %plot dos logs:
            subplot(2, 3, 1);

            %plot dos pontos condicionantes:
            plot(depth(conditioning_log),conditioning_log(conditioning_log),'*','color','red');
            %plot dos variogramas e histogramas:
            ranges_logs_of_inputs = [];





            tau=0.1; %0.001 at� 0.5
            delta=0.08; %0.001 at� 0.5   
            vmin=100; %1 at� 50
            spmin=3; %1 at� 20
            trend_range = 100;
        %     tau   = (0.5-0.001).*rand + 0.001;
        %     delta = (0.5-0.001).*rand + 0.001;
        %     vmin = (100-1).*rand + 1;
        %     spmin = (20-1).*rand + 1;
        %     trend_range =(1000-15).*rand + 10;

        annotation(figures(simu_id),'textbox',...
                [0.3963125 0.0498504486540379 0.2411875 0.168494516450647],...
                'String',{['realization: ',num2str(simu_id)],['tau: ',num2str(tau)],['delta: ',num2str(delta)],...
                ['vmin: ',num2str(vmin)],['spmin: ',num2str(spmin)],['trend range: ',num2str(trend_range)]}, 'FitBoxToText','off');
            pause(0.000000000001);
        %      try
                means=[]; 
                num_realiz=1;
                saidas = sgs_play(conditioning_log,original_logs,depth,...
                tau,delta,vmin,spmin,trend_range,means,num_realiz,nome_arquivo_treinamento,com_silicato);
                save(nome_arquivo_saida,'saidas');
                simulou = 1;
        end
        
    catch
        simulou = 0;
    end
end



simulou = 0;
while ~simulou
    try
        clear;close all;
        % well = load('wells/well_lucas1.mat');
        com_silicato = 0;
        propertie = {'VP','RHOB'};
        nome_arquivo_treinamento = 'saidaslucas2/vp_rho_treinamento.mat';
        nome_arquivo_saida = 'saidaslucas2/vp_rho.mat';
        
        well = read_las2_file('Well_1.las');

        columns = well.curve_info(:,1);

        
        properties_index = find(ismember(columns,propertie));
        output = well.curves(:,properties_index);

        depth = well.curves(:,find(ismember(columns,'DEPTH')));


        original_logs = output;
        %% %plot
        figure1=figure('units','normalized','outerposition',[0 0 1 1]);
        subplot(2, 3, 1);
        indexes = ~sum(isnan(original_logs)&original_logs==-999,2);
        for plot_id = 1:size(original_logs,2)
            plot(depth(indexes),original_logs(indexes,plot_id));
            hold on;
        end
        %% defini��o de pontos condicioantes:
        conditioning_log = zeros(size(depth));

        indices = ~logical(sum(isnan(output)|output==-999,2));
        conditioning_log(indices) = 1;

        conditioning_log = logical(conditioning_log);
        %% plotar dos pontos condicionantes:
        plot(depth(conditioning_log),conditioning_log(conditioning_log),'*','color','red');
        %% plot dos variogramas e histogramas


        %% simula��o
        num_sims = 1;
        figures = {};
        range_differences = [];
        for simu_id=1:num_sims
            close all;
            figures = [figures,figure('units','normalized','outerposition',[0 0 1 1])];
            %plot dos logs:
            subplot(2, 3, 1);

            %plot dos pontos condicionantes:
            plot(depth(conditioning_log),conditioning_log(conditioning_log),'*','color','red');
            %plot dos variogramas e histogramas:
            ranges_logs_of_inputs = [];





            tau=0.1; %0.001 at� 0.5
            delta=0.08; %0.001 at� 0.5   
            vmin=100; %1 at� 50
            spmin=3; %1 at� 20
            trend_range = 100;
        %     tau   = (0.5-0.001).*rand + 0.001;
        %     delta = (0.5-0.001).*rand + 0.001;
        %     vmin = (100-1).*rand + 1;
        %     spmin = (20-1).*rand + 1;
        %     trend_range =(1000-15).*rand + 10;

        annotation(figures(simu_id),'textbox',...
                [0.3963125 0.0498504486540379 0.2411875 0.168494516450647],...
                'String',{['realization: ',num2str(simu_id)],['tau: ',num2str(tau)],['delta: ',num2str(delta)],...
                ['vmin: ',num2str(vmin)],['spmin: ',num2str(spmin)],['trend range: ',num2str(trend_range)]}, 'FitBoxToText','off');
            pause(0.000000000001);
        %      try
                means=[]; 
                num_realiz=1;
                saidas = sgs_play(conditioning_log,original_logs,depth,...
                tau,delta,vmin,spmin,trend_range,means,num_realiz,nome_arquivo_treinamento,com_silicato);
                save(nome_arquivo_saida,'saidas');
        end
        simulou = 1;
    catch
        simulou = 0;
    end
end



simulou = 0;
while ~simulou
    try
        clear;close all;
        % well = load('wells/well_lucas1.mat');
        well = read_las2_file('Well_1.las');
        com_silicato = 0;
        propertie = {'VS','RHOB'};
        nome_arquivo_treinamento = 'saidaslucas2/vs_rho_treinamento.mat';
        nome_arquivo_saida = 'saidaslucas2/vs_rho.mat';
        columns = well.curve_info(:,1);

        properties_index = find(ismember(columns,propertie));
        output = well.curves(:,properties_index);

        depth = well.curves(:,find(ismember(columns,'DEPTH')));


        original_logs = output;
        %% %plot
        figure1=figure('units','normalized','outerposition',[0 0 1 1]);
        subplot(2, 3, 1);
        indexes = ~sum(isnan(original_logs)&original_logs==-999,2);
        for plot_id = 1:size(original_logs,2)
            plot(depth(indexes),original_logs(indexes,plot_id));
            hold on;
        end
        %% defini��o de pontos condicioantes:
        conditioning_log = zeros(size(depth));

        indices = ~logical(sum(isnan(output)|output==-999,2));
        conditioning_log(indices) = 1;

        conditioning_log = logical(conditioning_log);
        %% plotar dos pontos condicionantes:
        plot(depth(conditioning_log),conditioning_log(conditioning_log),'*','color','red');
        %% plot dos variogramas e histogramas


        %% simula��o
        num_sims = 1;
        figures = {};
        range_differences = [];
        for simu_id=1:num_sims
            close all;
            figures = [figures,figure('units','normalized','outerposition',[0 0 1 1])];
            %plot dos logs:
            subplot(2, 3, 1);

            %plot dos pontos condicionantes:
            plot(depth(conditioning_log),conditioning_log(conditioning_log),'*','color','red');
            %plot dos variogramas e histogramas:
            ranges_logs_of_inputs = [];





            tau=0.1; %0.001 at� 0.5
            delta=0.08; %0.001 at� 0.5   
            vmin=100; %1 at� 50
            spmin=3; %1 at� 20
            trend_range = 100;
        %     tau   = (0.5-0.001).*rand + 0.001;
        %     delta = (0.5-0.001).*rand + 0.001;
        %     vmin = (100-1).*rand + 1;
        %     spmin = (20-1).*rand + 1;
        %     trend_range =(1000-15).*rand + 10;

        annotation(figures(simu_id),'textbox',...
                [0.3963125 0.0498504486540379 0.2411875 0.168494516450647],...
                'String',{['realization: ',num2str(simu_id)],['tau: ',num2str(tau)],['delta: ',num2str(delta)],...
                ['vmin: ',num2str(vmin)],['spmin: ',num2str(spmin)],['trend range: ',num2str(trend_range)]}, 'FitBoxToText','off');
            pause(0.000000000001);
        %      try
                means=[]; 
                num_realiz=1;
                saidas = sgs_play(conditioning_log,original_logs,depth,...
                tau,delta,vmin,spmin,trend_range,means,num_realiz,nome_arquivo_treinamento,com_silicato);
                save(nome_arquivo_saida,'saidas');
        end
        simulou = 1;
    catch
        simulou = 0;
    end
end



simulou = 0;
while ~simulou
    try
        clear;close all;
        % well = load('wells/well_lucas1.mat');
        well = read_las2_file('Well_1.las');
        com_silicato = 1;
        propertie = {'VP','VS','RHOB'};
        nome_arquivo_treinamento = 'saidaslucas2/comsilicato/vp_vs_rho_treinamento.mat';
        nome_arquivo_saida = 'saidaslucas2/comsilicato/vp_vs_rho.mat';
        columns = well.curve_info(:,1);

        properties_index = find(ismember(columns,propertie));
        output = well.curves(:,properties_index);

        depth = well.curves(:,find(ismember(columns,'DEPTH')));


        original_logs = output;
        %% %plot
        figure1=figure('units','normalized','outerposition',[0 0 1 1]);
        subplot(2, 3, 1);
        indexes = ~sum(isnan(original_logs)&original_logs==-999,2);
        for plot_id = 1:size(original_logs,2)
            plot(depth(indexes),original_logs(indexes,plot_id));
            hold on;
        end
        %% defini��o de pontos condicioantes:
        conditioning_log = zeros(size(depth));

        indices = ~logical(sum(isnan(output)|output==-999,2));
        conditioning_log(indices) = 1;

        conditioning_log = logical(conditioning_log);
        %% plotar dos pontos condicionantes:
        plot(depth(conditioning_log),conditioning_log(conditioning_log),'*','color','red');
        %% plot dos variogramas e histogramas


        %% simula��o
        num_sims = 1;
        figures = {};
        range_differences = [];
        for simu_id=1:num_sims
            close all;
            figures = [figures,figure('units','normalized','outerposition',[0 0 1 1])];
            %plot dos logs:
            subplot(2, 3, 1);

            %plot dos pontos condicionantes:
            plot(depth(conditioning_log),conditioning_log(conditioning_log),'*','color','red');
            %plot dos variogramas e histogramas:
            ranges_logs_of_inputs = [];





            tau=0.1; %0.001 at� 0.5
            delta=0.08; %0.001 at� 0.5   
            vmin=100; %1 at� 50
            spmin=3; %1 at� 20
            trend_range = 100;
        %     tau   = (0.5-0.001).*rand + 0.001;
        %     delta = (0.5-0.001).*rand + 0.001;
        %     vmin = (100-1).*rand + 1;
        %     spmin = (20-1).*rand + 1;
        %     trend_range =(1000-15).*rand + 10;

        annotation(figures(simu_id),'textbox',...
                [0.3963125 0.0498504486540379 0.2411875 0.168494516450647],...
                'String',{['realization: ',num2str(simu_id)],['tau: ',num2str(tau)],['delta: ',num2str(delta)],...
                ['vmin: ',num2str(vmin)],['spmin: ',num2str(spmin)],['trend range: ',num2str(trend_range)]}, 'FitBoxToText','off');
            pause(0.000000000001);
        %      try
                means=[]; 
                num_realiz=1;
                saidas = sgs_play(conditioning_log,original_logs,depth,...
                tau,delta,vmin,spmin,trend_range,means,num_realiz,nome_arquivo_treinamento,com_silicato);
                save(nome_arquivo_saida,'saidas');
        end
        simulou = 1;
    catch
        simulou = 0;
    end
end



simulou = 0;
while ~simulou
    try
        clear;close all;
        % well = load('wells/well_lucas1.mat');
        well = read_las2_file('Well_1.las');
        com_silicato = 1;
        propertie = {'VS','RHOB'};
        nome_arquivo_treinamento = 'saidaslucas2/comsilicato/vs_rho_treinamento.mat';
        nome_arquivo_saida = 'saidaslucas2/comsilicato/vs_rho.mat';
        columns = well.curve_info(:,1);

 
        properties_index = find(ismember(columns,propertie));
        output = well.curves(:,properties_index);

        depth = well.curves(:,find(ismember(columns,'DEPTH')));


        original_logs = output;
        %% %plot
        figure1=figure('units','normalized','outerposition',[0 0 1 1]);
        subplot(2, 3, 1);
        indexes = ~sum(isnan(original_logs)&original_logs==-999,2);
        for plot_id = 1:size(original_logs,2)
            plot(depth(indexes),original_logs(indexes,plot_id));
            hold on;
        end
        %% defini��o de pontos condicioantes:
        conditioning_log = zeros(size(depth));

        indices = ~logical(sum(isnan(output)|output==-999,2));
        conditioning_log(indices) = 1;

        conditioning_log = logical(conditioning_log);
        %% plotar dos pontos condicionantes:
        plot(depth(conditioning_log),conditioning_log(conditioning_log),'*','color','red');
        %% plot dos variogramas e histogramas


        %% simula��o
        num_sims = 1;
        figures = {};
        range_differences = [];
        for simu_id=1:num_sims
            close all;
            figures = [figures,figure('units','normalized','outerposition',[0 0 1 1])];
            %plot dos logs:
            subplot(2, 3, 1);

            %plot dos pontos condicionantes:
            plot(depth(conditioning_log),conditioning_log(conditioning_log),'*','color','red');
            %plot dos variogramas e histogramas:
            ranges_logs_of_inputs = [];





            tau=0.1; %0.001 at� 0.5
            delta=0.08; %0.001 at� 0.5   
            vmin=100; %1 at� 50
            spmin=3; %1 at� 20
            trend_range = 100;
        %     tau   = (0.5-0.001).*rand + 0.001;
        %     delta = (0.5-0.001).*rand + 0.001;
        %     vmin = (100-1).*rand + 1;
        %     spmin = (20-1).*rand + 1;
        %     trend_range =(1000-15).*rand + 10;

        annotation(figures(simu_id),'textbox',...
                [0.3963125 0.0498504486540379 0.2411875 0.168494516450647],...
                'String',{['realization: ',num2str(simu_id)],['tau: ',num2str(tau)],['delta: ',num2str(delta)],...
                ['vmin: ',num2str(vmin)],['spmin: ',num2str(spmin)],['trend range: ',num2str(trend_range)]}, 'FitBoxToText','off');
            pause(0.000000000001);
        %      try
                means=[]; 
                num_realiz=1;
                saidas = sgs_play(conditioning_log,original_logs,depth,...
                tau,delta,vmin,spmin,trend_range,means,num_realiz,nome_arquivo_treinamento,com_silicato);
                save(nome_arquivo_saida,'saidas');
        end
        simulou = 1;
    catch
        simulou = 0;
    end
end



simulou = 0;
while ~simulou
    try
        clear;close all;
        % well = load('wells/well_lucas1.mat');
        well = read_las2_file('Well_1.las');
        com_silicato = 1;
        propertie = {'VP','RHOB'};
        nome_arquivo_treinamento = 'saidaslucas2/comsilicato/vp_rho_treinamento.mat';
        nome_arquivo_saida = 'saidaslucas2/comsilicato/vp_rho.mat';
        columns = well.curve_info(:,1);

        properties_index = find(ismember(columns,propertie));
        output = well.curves(:,properties_index);

        depth = well.curves(:,find(ismember(columns,'DEPTH')));


        original_logs = output;
        %% %plot
        figure1=figure('units','normalized','outerposition',[0 0 1 1]);
        subplot(2, 3, 1);
        indexes = ~sum(isnan(original_logs)&original_logs==-999,2);
        for plot_id = 1:size(original_logs,2)
            plot(depth(indexes),original_logs(indexes,plot_id));
            hold on;
        end
        %% defini��o de pontos condicioantes:
        conditioning_log = zeros(size(depth));

        indices = ~logical(sum(isnan(output)|output==-999,2));
        conditioning_log(indices) = 1;

        conditioning_log = logical(conditioning_log);
        %% plotar dos pontos condicionantes:
        plot(depth(conditioning_log),conditioning_log(conditioning_log),'*','color','red');
        %% plot dos variogramas e histogramas


        %% simula��o
        num_sims = 1;
        figures = {};
        range_differences = [];
        for simu_id=1:num_sims
            close all;
            figures = [figures,figure('units','normalized','outerposition',[0 0 1 1])];
            %plot dos logs:
            subplot(2, 3, 1);

            %plot dos pontos condicionantes:
            plot(depth(conditioning_log),conditioning_log(conditioning_log),'*','color','red');
            %plot dos variogramas e histogramas:
            ranges_logs_of_inputs = [];





            tau=0.1; %0.001 at� 0.5
            delta=0.08; %0.001 at� 0.5   
            vmin=100; %1 at� 50
            spmin=3; %1 at� 20
            trend_range = 100;
        %     tau   = (0.5-0.001).*rand + 0.001;
        %     delta = (0.5-0.001).*rand + 0.001;
        %     vmin = (100-1).*rand + 1;
        %     spmin = (20-1).*rand + 1;
        %     trend_range =(1000-15).*rand + 10;

        annotation(figures(simu_id),'textbox',...
                [0.3963125 0.0498504486540379 0.2411875 0.168494516450647],...
                'String',{['realization: ',num2str(simu_id)],['tau: ',num2str(tau)],['delta: ',num2str(delta)],...
                ['vmin: ',num2str(vmin)],['spmin: ',num2str(spmin)],['trend range: ',num2str(trend_range)]}, 'FitBoxToText','off');
            pause(0.000000000001);
        %      try
                means=[]; 
                num_realiz=1;
                saidas = sgs_play(conditioning_log,original_logs,depth,...
                tau,delta,vmin,spmin,trend_range,means,num_realiz,nome_arquivo_treinamento,com_silicato);
                save(nome_arquivo_saida,'saidas');
        end
        simulou = 1;
    catch
        simulou = 0;
    end
end