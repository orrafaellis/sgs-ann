function [out] = sgsigmn_patrec(conditioning_log,original_logs,depth_log,...
tau,delta,vmin,spmin,trend_range,means,num_realiz)
%grade:
out_simulation = -999*ones(size(depth_log,1),1+size(original_logs,2));
out_simulation(:,1)= depth_log;
% out_simulation(conditioning_log,2:end) = original_logs(conditioning_log,:);
%ajuste dos dados:
training_data = [depth_log(conditioning_log,:), original_logs(conditioning_log,:)];
%configura��o
range = max(training_data) - min(training_data);
options = [];
options.Display = 'final';
out = {};
modelo = 1;
for realization_id = 1:num_realiz
    switch modelo
        case 1
            model = igmn('CovType', 'full', 'range', range, 'tau',tau, 'delta', delta, 'vmin', vmin, 'spmin',...
                spmin, 'Options', options,'corr_coef',0,'range_trend',trend_range,'target_means',means);
        case 2
            model = siigmn('range', range,'CovType', 'full', 'tau', tau, 'delta', delta, 'vmin', 0, 'spmin', 0, 'Options', options,'trend_range',trend_range);
    end
%      [training_data(:,2) inscore_pars] = nscore(training_data(:,2));
    model = model.train(training_data);

    %simula��o
    
    step =100;
    available_points_index = ~conditioning_log;
    available_points = find(available_points_index);
    out = zeros(length(conditioning_log),2);
    out(conditioning_log,:) = training_data;
    out(:,1)= depth_log;
    figure;
    points_tosim = (round(sum(available_points_index)/step)-1);
    for step_id = 0:points_tosim
        
        subplot(2,1,2);
        cla
        x = out(~available_points_index,2);
        x= x - mean(x);
        covariogram = (( ifft( fft(x).*conj(fft(x)) ) )/length(x));
        covariogram = covariogram / max(covariogram);
        plot(covariogram);
        title(num2str(get_cpnoise_range(out(~available_points_index,2),1)));
        
        subplot(2,1,1);
        cla
        plot(out(~available_points_index,1),out(~available_points_index,2));
        
        
        
        
        
        hold on;
%         for cluster_id = 1:model.nc
%                 mean_i = model.means(cluster_id,:);
%                 cov_i = model.covs(:,:,cluster_id);
%                 plot(mean_i(1),mean_i(2),'v','color',[201, 34, 227]/255);
%                 plot_gaussian_ellipsoid(mean_i, cov_i);
%         end
         previous_pattern_indexes = available_points(step*step_id+1:(step_id+1)*step)-step; 
         plot(out(previous_pattern_indexes,1),out(previous_pattern_indexes,2),'color','red');
         %
        index_in_trained = mps(training_data(1:end-step,2)',out(previous_pattern_indexes,2)'+0.5*rand(size(out(previous_pattern_indexes,2)'))-(0.5/2)*ones(size(out(previous_pattern_indexes,2)')))+step;
        plot(training_data(index_in_trained,1),training_data(index_in_trained,2),'color','blue');
        switch modelo
            case 1
                
            stimated_ps = [];
            for point_id = 1:step
                %estimar media e cov
                try
                    dist_params = model.recall5(depth_log(index_in_trained(point_id)));
                catch
                    a=1;
                end
                    medias = dist_params{1};
                log_point = mvnrnd(medias,dist_params{2},1);
     

    %             model = model.train([depth_log(index_in_trained(point_id)),log_point]);
                stimated_ps = [stimated_ps;log_point];
            end
            
 
            case 2
                [medias vars] = model.recall_v(depth_log(index_in_trained));
                
                stimated_ps = mvnrnd(medias,vars(:)',1);
       end
        out(previous_pattern_indexes+step,2) = stimated_ps;
        plot(out(previous_pattern_indexes+step,1),out(previous_pattern_indexes+step,2))
        
        available_points_index(previous_pattern_indexes+step) = 0;
        pause(1);
    end
    
    
    
    
    available_points_index = logical(ones(length(conditioning_log),1));
    num_points_sim = 1000;
    start_p = 1;
    available_points = (start_p):(start_p+num_points_sim-1);
    estimative_points = create_pattern(find(conditioning_log), 1000, 3);
    simulated = [];
    figure;
    for sim_id = 1:num_points_sim
        

        hold on;
        actual_p = available_points(sim_id);
        sim_p = estimative_points(sim_id);

        %estimar media e cov
        dist_params = model.recall4(depth_log(sim_p));
        medias = dist_params{1};
        log_point = mvnrnd(medias,dist_params{2},1);
          
        
        trend_position = dist_params{3}{1};
%         plot(trend_position(1),trend_position(2),'p','color',[100, 34, 100]/255);
        
        out_simulation(actual_p,2:end) = log_point;
        simulated = [simulated;depth_log(actual_p),log_point];
        %adicionar ponto � rede:
        model = model.train(out_simulation(actual_p,:));
        plot(simulated(:,1),simulated(:,2),'*','color','black');
        pause(0.0000001)
        cla;
        if  sim_id > 10
            get_cpnoise_range(simulated(:,2),1)
        end
    end
%     out_simulation(:,2) = inscore(out_simulation(:,2),inscore_pars);
    out = [out,out_simulation((flip(available_points))',:)];
end
end

