%teste MPS
close all;
sizex = 1000;
range = 3;
x = 4*pi*linspace(0,1,sizex);
y= sin(x);
% y = fftma_l3c(1,sizex, range, range,0);

figure;plot(x,y,'color','black');
figure;
ya = y;
for i = 1:1000
    cla;
    num_points_complete = 10;
%          a = mps(y+0.1*rand(size(y)),y(end-num_points_complete+1:end));
    a = mps(ya(1:end-num_points_complete),ya(end-num_points_complete+1:end));
    index = a+num_points_complete;
    %precisa ver os limites:
    index(index < 1) = [];
    index(index > length(ya)) = [];
    ya = [ya, ya(index)];
    
    plot(ya,'color','black');
    pause(0.01);
    get_cpnoise_range(ya,1)
end
