classdef igmn
    
    properties
        name = 'IGMN';
        
        %matrix de covariancia precisa ser full, para performar a rota��o;
        
        % configuration params
        SIGMA = [];
        delta;
        tau;
        maxDist;
        spmin;
        vmin;
        uniform = false;
        covType;
        regVal;
        options;
        ang_rot;
        range_trend;
        target_means;
        
       
        % components params
        priors = [];
        means = [];
        covs = [];
        sps = [];
        vs = [];
        nc = 0;
  
        % Mahalanobis distance
        mahalaD;
        
        % sample size
        sampleSize = 0;
        
        % model likelihood
        dataLikelihood = 0;
        
        % information criteria 
        BIC;
        AIC;
        
        % components outputs
        loglike = [];
        post = [];
    end
    
    methods
        
        %Constructor
        function obj = igmn(varargin)
           
            pnames = {'range' 'sigma' 'delta' 'tau' 'vmin' 'spmin' 'uniform' 'covtype' 'regularize' 'options' 'corr_coef' 'range_trend' 'target_means'};
            dflts =  {[] [] 0.01 0.1 3 2 false 'full' 0 [] 0 100 []};
            [range sigma delta tau vmin spmin uniform covType regV options corr_coef range_trend target_means] ...
                = internal.stats.parseArgs(pnames, dflts, varargin{:});
             
            options = statset(statset('gmdistribution'), options);
            
            if ~isnumeric(delta) || ~isscalar(delta) || delta < 0 || delta > 1
                error('Invalid delta.');
            end
            obj.delta = delta;
            
            if isnumeric(corr_coef)
                obj.ang_rot = atan(-corr_coef);
            else
                error('Correlation Coef. parameter must be numeric!');
            end
            

            obj.target_means = target_means;

            
            if isnumeric(range_trend)
                obj.range_trend = range_trend;
            else
                error('Trend Range parameter must be numeric!');
            end
            
            if ~isempty(range) 
                if ~isrow(range)
                    error('Invalid range.');
                end
                dimension = size(range,2);
                obj.vmin = 2 * dimension;
                obj.spmin = dimension + 1;
                obj.SIGMA = (obj.delta * range).^2;
            else
                error('Range cannot be empty.');
            end
            
            if ~isempty(sigma) 
                if ~isrow(sigma)
                    error('Invalid sigma.');
                end
                obj.SIGMA = sigma;
            end
            
            if isempty(obj.SIGMA)
                warning('You must set the param SIGMA manually.');
            end
            
            if ~isnumeric(tau) || ~isscalar(tau) || tau < 0 || tau > 1
                error('Invalid tau.');
            end
            obj.tau = tau;
            obj.maxDist = chi2inv(1 - obj.tau, dimension);
            
            if ~isnumeric(vmin) || ~isscalar(vmin) || vmin < 0 
                error('Invalid vmin.');
            end
            obj.vmin = vmin;
            
            if ~isnumeric(spmin) || ~isscalar(spmin) || spmin < 0 
                error('Invalid spmin.');
            end
            obj.spmin = spmin;
            
            if ~islogical(uniform)
                error('Invalid uniform param.');
            end
            obj.uniform = uniform;
            
            if ischar(covType)
                covNames = {'diagonal', 'full'};
                i = find(strncmpi(covType, covNames, length(covType)));
                if isempty(i)
                    error('Invalid covariance type.');
                end
                CovType = i;
            else
                error('Invalid covariance type.');
            end
            obj.covType = CovType;

            if ~isnumeric(regV) || ~isscalar(regV) || regV < 0
                error('Invalid regularize value.');
            end
            obj.regVal = regV;
                                    
            options.Display = find(strncmpi(options.Display, {'off', 'final', 'iter'}, length(options.Display))) - 1;
            obj.options = options;
        end
        
        % Compute the log probability density of a multivariate normal distribution
        function [log_lh, mahalaD, logDetCov] = logmvnpdf(obj, X, means, covs)
            [n,d] = size(X);
            k = size(means,1);
            log_lh = zeros(n, k);
            mahalaD = zeros(n, k);
            logDetCov = zeros(1, k);
            
            for j = 1:k
                if obj.covType == 2 % full covariance
                    [L,f] = chol(covs(:,:,j));
                    diagL = diag(L);
                    if ((f ~= 0) || any(abs(diagL) < eps(max(abs(diagL))) * size(L,1)))
                        error('Ill Conditioned Covariance.');
                    end
                    logDetCov(j) = 2 * sum(log(diagL));
                 else % diagonal covariance
                    L = sqrt(covs(:,:,j)); % a vector 
                    if  any(L < eps(max(L)) * d)
                        error('Ill Conditioned Covariance.');
                    end
                    logDetCov(j) = sum(log(covs(:,:,j)));
                end
                Xcentered = bsxfun(@minus, X, means(j,:));
    
                if obj.covType == 2
                    xRinv = Xcentered / L;
                else
                    xRinv = bsxfun(@times, Xcentered , (1 ./ L));
                end
                
                mahalaD(:,j) = sum(xRinv.^2, 2);
                log_lh(:,j) = - 0.5 * (mahalaD(:,j) + logDetCov(j) + d * log(2 * pi));              
            end
        end
        
        % Compute the log probability density of the multivariate normal distribution
        % evaluated for each IGMN component
        function obj = computeLikelihood(obj, x)
            if obj.nc > 0
                [obj.loglike, obj.mahalaD] = obj.logmvnpdf(x, obj.means, obj.covs);
            end
        end
        
        % Compute the posterior probability for each IGMN component
        function obj = computePosterior(obj)
            logPrior = log(obj.priors);
            obj.post = bsxfun(@plus, obj.loglike, logPrior);
            maxll = max(obj.post, [], 2);
            % minus maxll to avoid underflow
            obj.post = exp(bsxfun(@minus, obj.post, maxll));
            density = sum(obj.post, 2);
            % normalize
            obj.post = bsxfun(@rdivide, obj.post, density);
            logpdf = log(density) + maxll;
            obj.dataLikelihood = obj.dataLikelihood + sum(logpdf);
        end
        
        % Check the IGMN novelty criterion
        function h = hasAcceptableDistribution(obj)
            for j = 1:obj.nc
            	if (obj.mahalaD(j) <= obj.maxDist)
                    h = true;
                    return;
                end
            end
            
            h = false;
        end
        
        % Create a component when a data point x matches the novelty criterion
        function obj = createComponent(obj, x)
            obj.nc = obj.nc + 1;
            obj.means(obj.nc,:) = x;
            obj.sps(1,obj.nc) = 1.0; %sps: prob a posteriori
            obj.vs(1,obj.nc) = 0; %idade do componente [nc]
            obj = obj.updatePriors();
            
            rot_matrix = [cos(obj.ang_rot) -sin(obj.ang_rot); sin(obj.ang_rot) cos(obj.ang_rot)];
            if obj.covType == 2
                cov_m = diag(obj.SIGMA);
%                  obj.covs(:,:,obj.nc) = rot_matrix*cov_m*(rot_matrix');
                    obj.covs(:,:,obj.nc) = cov_m;
            else
                 obj.covs(:,:,obj.nc) = obj.SIGMA;
            end
            [obj.loglike(1,obj.nc), obj.mahalaD(1, obj.nc)] = obj.logmvnpdf(x, obj.means(obj.nc,:), obj.covs(:,:,obj.nc));
        end
        
        % Update the IGMN priors
        function obj = updatePriors(obj)
            if ~obj.uniform
                obj.priors = obj.sps ./ sum(obj.sps);
             else
                obj.priors = ones(1,obj.nc) ./ obj.nc;
            end
        end
        
        % Update the IGMN parameters 
        function obj = updateComponents(obj, x)
            obj.sps = obj.sps + obj.post;
            obj.vs = obj.vs + 1;
            
            w = obj.post ./ obj.sps; 
           
            d = size(obj.means,2);
            for j = 1:obj.nc
                Xcentered = x - obj.means(j,:); %e
                deltaMU = w(j) * Xcentered;
                obj.means(j,:) = obj.means(j,:) + deltaMU; 
                
                XcenteredNEW = x - obj.means(j,:);    %e*

                if obj.covType == 2 % full covariance
                    obj.covs(:,:,j) = obj.covs(:,:,j) - deltaMU' * deltaMU + w(j) * (XcenteredNEW' * XcenteredNEW - obj.covs(:,:,j))...
                        + obj.regVal * eye(d);
                else % diagonal covariance 
                    obj.covs(:,:,j) = obj.covs(:,:,j) - deltaMU.^2 + w(j) * (XcenteredNEW.^2 - obj.covs(:,:,j))...
                        + obj.regVal;
                end
            end
            obj.covs(obj.covs < 0) = 0.0000001;
            
            % normalize the priors
            obj = obj.updatePriors();
        end 
        
        % Remove spurious components
        function obj = removeSpurious(obj)
            for i = obj.nc:-1:1
                if (obj.sps(i) < obj.spmin && obj.vs(i) > obj.vmin)
                    obj.nc = obj.nc - 1;
                    obj.priors(i) = [];
                    obj.means(i,:) = [];
                    obj.covs(:,:,i) = []; 
                    obj.vs(i) = [];
                    obj.sps(i) = [];                    
                    obj.post(i) = [];
                    obj.mahalaD(i) = [];
                    obj.loglike(i) = [];
                end
            end
        end
        function obj = train_point(obj, X)
            dispfmt = '%6d\t%12g';
            
            if obj.options.Display > 1 % 'iter'
                fprintf('  iter\t    log-likelihood\n');
            end
            
            N = size(X,1);
            
            for i = 1:N
                
          %      if (mod(i,100) == 0) fprintf('Training input: %d / %d - number of components: %d\n', i, N, obj.nc); end
     
                x = X(i,:);
                
                obj = obj.computeLikelihood(x);
                if (~obj.hasAcceptableDistribution())
                    obj = obj.createComponent(x);
                end
                obj = obj.computePosterior();
                
                obj = obj.updateComponents(x);
                obj = obj.removeSpurious();
                
                if obj.options.Display > 1 %'iter'
                    disp(sprintf(dispfmt, i, obj.dataLikelihood));
                end
    
            end
             
            if obj.options.Display > 0 % 'final' or 'iter'
                fprintf('%d iterations, log-likelihood = %g\n', i, obj.dataLikelihood);
            end
            
            obj.sampleSize = obj.sampleSize + N;
            
            obj = obj.computeInfoCriteria();
        end
        % Train the IGMN with the data set X
        function obj = train(obj, X)
            canceled_training = 0;
            dispfmt = '%6d\t%12g';
            
            if obj.options.Display > 1 % 'iter'
                fprintf('  iter\t    log-likelihood\n');
            end
            
            N = size(X,1);
            wbw=waitbar(0,{'';'Training IGMN Model';''},'Name', 'Training Progress', ...
  'CreateCancelBtn', 'setappdata(gcbf,''canceling'',1)');
            setappdata(wbw,'canceling',0);
            for i = 1:N
                waitbar(i/N,wbw);
                
                if isequal(mod(i-1,140),0)
                   switch mod(i-1,3)
                       case 0
                            wbw.Name = 'Training Progress...';
                       case 1
                            wbw.Name = 'Training Progress..';
                       case 2
                            wbw.Name = 'Training Progress.';
                       
                   end
                      
                end
                
          %      if (mod(i,100) == 0) fprintf('Training input: %d / %d - number of components: %d\n', i, N, obj.nc); end
     
                x = X(i,:);
                if getappdata(wbw,'canceling')
                    canceled_training = 1;
                    break
                end
                
                obj = obj.computeLikelihood(x);
                if (~obj.hasAcceptableDistribution())
                    obj = obj.createComponent(x);
                end
                obj = obj.computePosterior();
                
                obj = obj.updateComponents(x);
                obj = obj.removeSpurious();
                
                if obj.options.Display > 1 %'iter'
                    disp(sprintf(dispfmt, i, obj.dataLikelihood));
                end
                
            end
             
            if (obj.options.Display > 0)&&(~canceled_training) % 'final' or 'iter'
                fprintf('%d iterations, log-likelihood = %g\n', i, obj.dataLikelihood);
            end
            
            obj.sampleSize = obj.sampleSize + N;
            
            obj = obj.computeInfoCriteria();
            delete(wbw);
            if canceled_training
            	obj = []; 
            end
        end
        
        function B = recall_original(obj, X)
            if isempty(X)
                error('Input cannnot be empty.');
            end;
            if obj.nc > 0 && size(X,2) == size(obj.means, 2)
                error('Input vector must has the dimension of the vector <a> where x = [a,b] was the sample used for training and <b> is the label vector.');
            end;
            
            N = size(X, 1);
            B = zeros(N, size(obj.means,2) - size(X, 2));
            for j = 1:N
                x = X(j,:);
                
                alpha = size(x, 2);
                beta = size(obj.means, 2) - alpha;

                pajs = zeros(obj.nc, 1);
                xm = zeros(obj.nc, beta);

                for i=1:obj.nc
                    meanA = obj.means(i, 1:alpha);
                    meanB = obj.means(i, alpha+1:alpha+beta);

                    if obj.covType == 2 % full covariance
                        covA = obj.covs(1:alpha, 1:alpha, i);
                        covBA = obj.covs(alpha+1:alpha+beta, 1:alpha, i);
                       
                        xm(i,:) = meanB + (covBA / covA * (x - meanA)')';
                    else
                        covA = obj.covs(1:alpha); 
                       
                        xm(i,:) = meanB;
                    end
                    loglike = obj.logmvnpdf(x, meanA, covA);
                    pajs(i) = exp(loglike) * obj.priors(i);
                end

                pajs = pajs ./ sum(pajs);
                B(j,:) = sum(bsxfun(@times, xm, pajs));
            end
        end
        
        % IGMN recalling algorithm
        function out = recall(obj, X)
            if isempty(X)
                error('Input cannnot be empty.');
            end;
            if obj.nc > 0 && size(X,2) == size(obj.means, 2)
                error('Input vector must has the dimension of the vector <a> where x = [a,b] was the sample used for ing and <b> is the label vector.');
            end;
            
            N = size(X, 1);
            B = zeros(N, size(obj.means,2) - size(X, 2));

            %create trend gausssian:
            R = obj.range_trend;
            
            alpha = size(X, 2);
            beta = size(obj.means, 2) - alpha;
            closest_gaussian_id = find(ismember(abs(obj.means(:,1:alpha)-X),min(abs(obj.means(:,1:alpha)-X))));
            known_Cov = obj.covs(1:alpha, 1:alpha, closest_gaussian_id);
            knownGauss_Mean = obj.means(closest_gaussian_id, 1:alpha);
            
            mahaldist = (X-knownGauss_Mean)'*inv(known_Cov)*(X-knownGauss_Mean);
            mu_trend_i = X+norm(knownGauss_Mean-X)*R/mahaldist;
            
            if isempty(obj.target_means)
                mu_trend_t = mean(obj.means(:, alpha+1:alpha+beta));
            else
                mu_trend_t = obj.target_means;
            end
            
            %trabalhar como se [mu_trend_i,mu_trend_t] fosse um ponto p/
            %ser treinado
            
            obj.nc = obj.nc + 1;
            obj.means(obj.nc,:) = [mu_trend_i,mu_trend_t];
            obj.sps(1,obj.nc) = 1.0; %sps: prob a posteriori
            obj.vs(1,obj.nc) = 0; %idade do componente [nc]
            rot_matrix = [cos(obj.ang_rot) -sin(obj.ang_rot); sin(obj.ang_rot) cos(obj.ang_rot)];
            cov_m = diag(obj.SIGMA);
%             obj.covs(:,:,obj.nc) = rot_matrix*cov_m*(rot_matrix');
            obj.covs(:,:,obj.nc) = cov_m;
            obj.priors(obj.nc) = obj.priors(closest_gaussian_id);
            
%             obj = obj.updatePriors();
            
%               [obj.loglike(1,obj.nc), obj.mahalaD(1, obj.nc)] = obj.logmvnpdf([mu_trend_i,mu_trend_t], obj.means(obj.nc,:), obj.covs(:,:,obj.nc));
            
            
%               obj = obj.computePosterior();
%               obj = obj.updateComponents([mu_trend_i,mu_trend_t]);
            
            for j = 1:N
                x = X(j,:);
                
                alpha = size(x, 2);
                beta = size(obj.means, 2) - alpha;
                
                pajs = zeros(obj.nc, 1);
                xm = zeros(obj.nc, beta);
                means_gauss = [];
                
                for i=1:obj.nc
                    meanA = obj.means(i, 1:alpha);
                    meanB = obj.means(i, alpha+1:alpha+beta);

                    if obj.covType == 2 % full covariance
                        covA = obj.covs(1:alpha, 1:alpha, i);
                        covBA = obj.covs(alpha+1:alpha+beta, 1:alpha, i);
                        means_gauss = [means_gauss,meanB];
                        xm(i,:) = meanB + (covBA / covA * (x - meanA)')';
                    else
                        covA = obj.covs(1:alpha); 
                       
                        xm(i,:) = meanB;
                    end
                    loglike = obj.logmvnpdf(x, meanA, covA);
                    pajs(i) = exp(loglike) * obj.priors(i);
                end

                pajs = pajs ./ sum(pajs);
                B(j,:) = sum(bsxfun(@times, xm, pajs));
            end
            
            %calculate variance:
            means_gauss = mean(means_gauss);
            for j = 1:N
                x = X(j,:);
                
                alpha = size(x, 2);
                beta = size(obj.means, 2) - alpha;
                Cov = zeros(size(obj.means,2) - size(X, 2));
                for i=1:obj.nc
                    meanA = obj.means(i, 1:alpha);
                    meanB = obj.means(i, alpha+1:alpha+beta);
                    
                    covA = obj.covs(1:alpha, 1:alpha, i);
                    covBA = obj.covs(alpha+1:alpha+beta, 1:alpha, i);
                    covBB = obj.covs(alpha+1:alpha+beta,alpha+1:alpha+beta,i);
                     Cov = Cov+pajs(i)*(covBB-covBA*inv(covA)*covBA');
%                     Cov = Cov+pajs(i)*(covBB-covBA*inv(covA)*covBA' + norm(meanB-B(j,:)) );
                    if isnan(Cov)
                        aaaa=1;
                    end
                end

               
            end
%             out = [B, Cov];
%             Cov = Cov + norm(means_gauss-B(j,:));
            out = {B, Cov,{[mu_trend_i,mu_trend_t],obj.covs(:, :, end)}};
        end
        function out = recall2(obj, X)
            if isempty(X)
                error('Input cannnot be empty.');
            end;
            if obj.nc > 0 && size(X,2) == size(obj.means, 2)
                error('Input vector must has the dimension of the vector <a> where x = [a,b] was the sample used for ing and <b> is the label vector.');
            end;
            
            N = size(X, 1);
            B = zeros(N, size(obj.means,2) - size(X, 2));

            %create trend gausssian:
            R = obj.range_trend;
            
            alpha = size(X, 2);
            beta = size(obj.means, 2) - alpha;
            closest_gaussian_id = find(ismember(abs(obj.means(:,1:alpha)-X),min(abs(obj.means(:,1:alpha)-X))));
            known_Cov = obj.covs(1:alpha, 1:alpha, closest_gaussian_id);
            knownGauss_Mean = obj.means(closest_gaussian_id, 1:alpha);
            
            mahaldist = (X-knownGauss_Mean)'*inv(known_Cov)*(X-knownGauss_Mean);
            mu_trend_i = X+norm(knownGauss_Mean-X)*R/mahaldist;
            
            if isempty(obj.target_means)
                mu_trend_t = mean(obj.means(:, alpha+1:alpha+beta));
            else
                mu_trend_t = obj.target_means;
            end
            
            %trabalhar como se [mu_trend_i,mu_trend_t] fosse um ponto p/
            %ser treinado
            
            obj.nc = obj.nc + 1;
            obj.means(obj.nc,:) = [mu_trend_i,mu_trend_t];
            obj.sps(1,obj.nc) = 1.0; %sps: prob a posteriori
            obj.vs(1,obj.nc) = 0; %idade do componente [nc]
            rot_matrix = [cos(obj.ang_rot) -sin(obj.ang_rot); sin(obj.ang_rot) cos(obj.ang_rot)];
            cov_m = diag(obj.SIGMA);
%             obj.covs(:,:,obj.nc) = rot_matrix*cov_m*(rot_matrix');
            obj.covs(:,:,obj.nc) = cov_m;
%             obj.priors(obj.nc) = obj.priors(closest_gaussian_id);


            obj = obj.updatePriors();
            
%               [obj.loglike(1,obj.nc), obj.mahalaD(1, obj.nc)] = obj.logmvnpdf([mu_trend_i,mu_trend_t], obj.means(obj.nc,:), obj.covs(:,:,obj.nc));
            
            
%               obj = obj.computePosterior();
%               obj = obj.updateComponents([mu_trend_i,mu_trend_t]);
            
            for j = 1:N
                x = X(j,:);
                
                alpha = size(x, 2);
                beta = size(obj.means, 2) - alpha;
                
                pajs = zeros(obj.nc, 1);
                xm = zeros(obj.nc, beta);
                means_gauss = [];
                dist = zeros(obj.nc);
                for i=1:obj.nc
                    meanA = obj.means(i, 1:alpha);
                    meanB = obj.means(i, alpha+1:alpha+beta);

                    if obj.covType == 2 % full covariance
                        covA = obj.covs(1:alpha, 1:alpha, i);
                        covBA = obj.covs(alpha+1:alpha+beta, 1:alpha, i);
                        covBB = obj.covs(alpha+1:alpha+beta,alpha+1:alpha+beta,i);   
                        covBA_div_covA = covBA / covA;
                        covm(:,:,i) = covBB-covBA_div_covA*covBA';
                        
                        
                        xm(i,:) = meanB + (covBA / covA * (x - meanA)')';
                        
                    else
                        covA = obj.covs(1:alpha); 
                       
                        xm(i,:) = meanB;
                    end
                    [loglike,dist(i),~] = obj.logmvnpdf(x, meanA, covA);
                    pajs(i) = exp(loglike) * obj.priors(i);
                    
                end

                pajs = pajs ./ sum(pajs);
                B(j,:) = sum(bsxfun(@times, xm, pajs));
            end
            
            %calculate variance:
           
                %fprintf('meank\txmk\tBj\tpajs\tcovm\n');
                Cov = zeros(size(obj.means,2) - size(X, 2));
                variogram_range = 1;
                for k=1:obj.nc                    
                    contribution_to_variance_k = pajs(k)*(covm(:,:,k)+ sqrt(sum((xm(k,:)-B(j,:)).^2))^2);
                    
                    %Test 14/03/2016. Goal: reduce high frequency by making
                    %the variance proportional to the distance to known
                    %points:
                    
                    factor = (1-exp(-dist(k)/(variogram_range*10))); %exp
                    %factor = (3*dist(k))/(2*variogram_range) - (dist(k)^3)/(2*variogram_range^3);             
                    
                    contribution_to_variance_k = contribution_to_variance_k*factor;
                    
                    %contribution_to_variance_k = 0; 
                   
                    %Original variance                    
                   Cov(:,:,j) = Cov(:,:,j) + contribution_to_variance_k;
                end
            out = {B, Cov,{[mu_trend_i,mu_trend_t],obj.covs(:, :, end)}};
        end
         function out = recall3(obj, X)
            if isempty(X)
                error('Input cannnot be empty.');
            end;
            if obj.nc > 0 && size(X,2) == size(obj.means, 2)
                error('Input vector must has the dimension of the vector <a> where x = [a,b] was the sample used for ing and <b> is the label vector.');
            end;
            
            N = size(X, 1);
            B = zeros(N, size(obj.means,2) - size(X, 2));
            
            
            GLOBAL_VARIANCE=0;
            index=size(obj.means,2);
            for j=1:obj.nc
                if obj.covs(index,index,j) > GLOBAL_VARIANCE
                    GLOBAL_VARIANCE = obj.covs(index,index,j);
                end
            end
            GLOBAL_VARIANCE = GLOBAL_VARIANCE*2;
%             fprintf('\nGLOBAL_VARIANCE PARAMETER=%f\n',GLOBAL_VARIANCE);
% %             
            
            %create trend gausssian:
            R = obj.range_trend;
            
            alpha = size(X, 2);
            beta = size(obj.means, 2) - alpha;
            closest_gaussian_id = find(ismember(abs(obj.means(:,1:alpha)-X),min(abs(obj.means(:,1:alpha)-X))));
            known_Cov = obj.covs(1:alpha, 1:alpha, closest_gaussian_id);
            knownGauss_Mean = obj.means(closest_gaussian_id, 1:alpha);
            
            mahaldist = (X-knownGauss_Mean)'*inv(known_Cov)*(X-knownGauss_Mean);
            mu_trend_i = X+(knownGauss_Mean-X)*R/mahaldist;
            
            mu_trend_t = [];
            for mean_i = 1:length(obj.target_means)
                if isempty(obj.target_means{mean_i})
                    mu_trend_ti = mean(obj.means(:, alpha+1:alpha+beta));
                    mu_trend_t = [mu_trend_t,mu_trend_ti(mean_i)];
                else
                    mu_trend_t = [mu_trend_t,obj.target_means{mean_i}];
                end    
            end
            
            %trabalhar como se [mu_trend_i,mu_trend_t] fosse um ponto p/
            %ser treinado
            
            obj.nc = obj.nc + 1;
            obj.means(obj.nc,:) = [mu_trend_i,mu_trend_t];
            obj.sps(1,obj.nc) = 1.0; %sps: prob a posteriori
            obj.vs(1,obj.nc) = 0; %idade do componente [nc]
            rot_matrix = [cos(obj.ang_rot) -sin(obj.ang_rot); sin(obj.ang_rot) cos(obj.ang_rot)];
            cov_m = diag(obj.SIGMA);
%             obj.covs(:,:,obj.nc) = rot_matrix*cov_m*(rot_matrix');
            obj.covs(:,:,obj.nc) = cov_m;
%             obj.priors(obj.nc) = obj.priors(closest_gaussian_id);


            obj = obj.updatePriors();
            
              [obj.loglike(1,obj.nc), obj.mahalaD(1, obj.nc)] = obj.logmvnpdf([mu_trend_i,mu_trend_t], obj.means(obj.nc,:), obj.covs(:,:,obj.nc));
            
            
              obj = obj.computePosterior();
              obj = obj.updateComponents([mu_trend_i,mu_trend_t]);
            
            for j = 1:N
                x = X(j,:);
                
                alpha = size(x, 2);
                beta = size(obj.means, 2) - alpha;
                
                pajs = zeros(obj.nc, 1);
                xm = zeros(obj.nc, beta);
                means_gauss = [];
                dist = zeros(obj.nc);
                for i=1:obj.nc
                    meanA = obj.means(i, 1:alpha);
                    meanB = obj.means(i, alpha+1:alpha+beta);

                    if obj.covType == 2 % full covariance
                        covA = obj.covs(1:alpha, 1:alpha, i);
                        covBA = obj.covs(alpha+1:alpha+beta, 1:alpha, i);
                        covBB = obj.covs(alpha+1:alpha+beta,alpha+1:alpha+beta,i);   
                        covBA_div_covA = covBA / covA;
                        covm(:,:,i) = covBB-covBA_div_covA*covBA';
                        
                        
                        xm(i,:) = meanB + (covBA / covA * (x - meanA)')';
                        
                    else
                        covA = obj.covs(1:alpha); 
                       
                        xm(i,:) = meanB;
                    end
                    [loglike,dist(i),~] = obj.logmvnpdf(x, meanA, covA);
                    pajs(i) = exp(loglike) * obj.priors(i);
                    
                end

                pajs = pajs ./ sum(pajs);
                B(j,:) = sum(bsxfun(@times, xm, pajs));
            end
            
            %calculate variance:
           
                %fprintf('meank\txmk\tBj\tpajs\tcovm\n');
                Cov = zeros(size(obj.means,2) - size(X, 2));
                variogram_range = R;
                for k=1:obj.nc                    
                    contribution_to_variance_k = pajs(k)*(covm(:,:,k)+ sqrt(sum((xm(k,:)-B(j,:)).^2))^2);
                    
                    %Test 14/03/2016. Goal: reduce high frequency by making
                    %the variance proportional to the distance to known
                    %points:
                    
                    factor = (1-exp(-dist(k)/(variogram_range*10))); %exp
                    %factor = (3*dist(k))/(2*variogram_range) - (dist(k)^3)/(2*variogram_range^3);             
                    
                    contribution_to_variance_k = contribution_to_variance_k*factor;
                    
                    %contribution_to_variance_k = 0; 
                   
                    %Original variance                    
                   Cov(:,:,j) = Cov(:,:,j) + contribution_to_variance_k;
                end
                
                if(mahaldist > variogram_range)
                    Cov(:,:,j) = GLOBAL_VARIANCE;
                end
                
                
                
            out = {B, Cov,{[mu_trend_i,mu_trend_t],obj.covs(:, :, end)}};
        end
        function out = recall4(obj, X)
            if isempty(X)
                error('Input cannnot be empty.');
            end;
            if obj.nc > 0 && size(X,2) == size(obj.means, 2)
                error('Input vector must has the dimension of the vector <a> where x = [a,b] was the sample used for ing and <b> is the label vector.');
            end;
            
            N = size(X, 1);
            B = zeros(N, size(obj.means,2) - size(X, 2));
            
            
            GLOBAL_VARIANCE=0;
            index=size(obj.means,2);
            for j=1:obj.nc
                if obj.covs(index,index,j) > GLOBAL_VARIANCE
                    GLOBAL_VARIANCE = obj.covs(index,index,j);
                end
            end
            GLOBAL_VARIANCE = GLOBAL_VARIANCE*2;
%             fprintf('\nGLOBAL_VARIANCE PARAMETER=%f\n',GLOBAL_VARIANCE);
% %             
            
            %create trend gausssian:
            R = obj.range_trend;
            
            alpha = size(X, 2);
            beta = size(obj.means, 2) - alpha;
            closest_gaussian_id = find(ismember(abs(obj.means(:,1:alpha)-X),min(abs(obj.means(:,1:alpha)-X))));
            known_Cov = obj.covs(1:alpha, 1:alpha, closest_gaussian_id);
            knownGauss_Mean = obj.means(closest_gaussian_id, 1:alpha);
            
            mahaldist = (X-knownGauss_Mean)'*inv(known_Cov)*(X-knownGauss_Mean);
            mu_trend_i = X+(knownGauss_Mean-X)*R/mahaldist;
            
            mu_trend_t = [];
            for mean_i = 1:length(obj.target_means)
                if isempty(obj.target_means{mean_i})
                    mu_trend_ti = mean(obj.means(:, alpha+1:alpha+beta));
                    mu_trend_t = [mu_trend_t,mu_trend_ti(mean_i)];
                else
                    mu_trend_t = [mu_trend_t,obj.target_means{mean_i}];
                end    
            end
            
            
            %trabalhar como se [mu_trend_i,mu_trend_t] fosse um ponto p/
            %ser treinado
            
            obj.nc = obj.nc + 1;
            obj.means(obj.nc,:) = [mu_trend_i,mu_trend_t];
            obj.sps(1,obj.nc) = 1.0; %sps: prob a posteriori
            obj.vs(1,obj.nc) = 0; %idade do componente [nc]
            rot_matrix = [cos(obj.ang_rot) -sin(obj.ang_rot); sin(obj.ang_rot) cos(obj.ang_rot)];
            cov_m = diag(obj.SIGMA);
%             obj.covs(:,:,obj.nc) = rot_matrix*cov_m*(rot_matrix');
            obj.covs(:,:,obj.nc) = cov_m;
%             obj.priors(obj.nc) = obj.priors(closest_gaussian_id);


            obj = obj.updatePriors();
            
              [obj.loglike(1,obj.nc), obj.mahalaD(1, obj.nc)] = obj.logmvnpdf([mu_trend_i,mu_trend_t], obj.means(obj.nc,:), obj.covs(:,:,obj.nc));
            
            
              obj = obj.computePosterior();
              obj = obj.updateComponents([mu_trend_i,mu_trend_t]);
            
            for j = 1:N
                x = X(j,:);
                
                alpha = size(x, 2);
                beta = size(obj.means, 2) - alpha;
                
                pajs = zeros(obj.nc, 1);
                xm = zeros(obj.nc, beta);
                means_gauss = [];
                dist = zeros(obj.nc);
                for i=1:obj.nc
                    meanA = obj.means(i, 1:alpha);
                    meanB = obj.means(i, alpha+1:alpha+beta);

                    if obj.covType == 2 % full covariance
                        covA = obj.covs(1:alpha, 1:alpha, i);
                        covBA = obj.covs(alpha+1:alpha+beta, 1:alpha, i);
                        covBB = obj.covs(alpha+1:alpha+beta,alpha+1:alpha+beta,i);   
                        covBA_div_covA = covBA / covA;
                        covm(:,:,i) = covBB-covBA_div_covA*covBA';
                        
                        
                        xm(i,:) = meanB + (covBA / covA * (x - meanA)')';
                        
                    else
                        covA = obj.covs(1:alpha); 
                       
                        xm(i,:) = meanB;
                    end
                    [loglike,dist(i),~] = obj.logmvnpdf(x, meanA, covA);
                    pajs(i) = exp(loglike) * obj.priors(i);
                    
                end
                
                %controle da correla��o espacial:
                [~,indexes_moreProbGauss]=sort(pajs);
                indexes_moreProbGauss=flip(indexes_moreProbGauss);
                pajs_r = pajs(indexes_moreProbGauss);
                
                window_factor = mahaldist/R;
                
                
                windowss = round(obj.nc*window_factor);
                w = gausswin(windowss);
                if windowss>0
                    pajs_r = filter(w,1,pajs_r);
                end
                
                pajs_r(indexes_moreProbGauss) = pajs_r;
                pajs = pajs_r ./ sum(pajs_r);
%                 pajs = pajs ./ sum(pajs);
                
                
                
                
                
                B(j,:) = sum(bsxfun(@times, xm, pajs));
            end
            
            %calculate variance:
           
                %fprintf('meank\txmk\tBj\tpajs\tcovm\n');
                Cov = zeros(size(obj.means,2) - size(X, 2));
                variogram_range = R;
                for k=1:obj.nc                    
                    contribution_to_variance_k = pajs(k)*(covm(:,:,k)+ sqrt(sum((xm(k,:)-B(j,:)).^2))^2);
                    
                    %Test 14/03/2016. Goal: reduce high frequency by making
                    %the variance proportional to the distance to known
                    %points:
                    
                    factor = (1-exp(-dist(k)/(variogram_range*10))); %exp
                    %factor = (3*dist(k))/(2*variogram_range) - (dist(k)^3)/(2*variogram_range^3);             
                    
                    contribution_to_variance_k = contribution_to_variance_k*factor;
                    
                    %contribution_to_variance_k = 0; 
                   
                    %Original variance                    
                   Cov(:,:,j) = Cov(:,:,j) + contribution_to_variance_k;
                end
                
%                 if(mahaldist > variogram_range)
%                     Cov(:,:,j) = GLOBAL_VARIANCE;
%                 end
                
                
                
            out = {B, Cov,{[mu_trend_i,mu_trend_t],obj.covs(:, :, end)}};
        end
        function out = recall5(obj, X)
            if isempty(X)
                error('Input cannnot be empty.');
            end;
            if obj.nc > 0 && size(X,2) == size(obj.means, 2)
                error('Input vector must has the dimension of the vector <a> where x = [a,b] was the sample used for ing and <b> is the label vector.');
            end;
            
            N = size(X, 1);
            B = zeros(N, size(obj.means,2) - size(X, 2));
            
            
            GLOBAL_VARIANCE=0;
            index=size(obj.means,2);
            for j=1:obj.nc
                if obj.covs(index,index,j) > GLOBAL_VARIANCE
                    GLOBAL_VARIANCE = obj.covs(index,index,j);
                end
            end
            GLOBAL_VARIANCE = GLOBAL_VARIANCE*2;
%             fprintf('\nGLOBAL_VARIANCE PARAMETER=%f\n',GLOBAL_VARIANCE);
% %             
            
            %create trend gausssian:
            R = obj.range_trend;
            
            alpha = size(X, 2);
            beta = size(obj.means, 2) - alpha;
            closest_gaussian_id = find(ismember(abs(obj.means(:,1:alpha)-X),min(abs(obj.means(:,1:alpha)-X))));
            known_Cov = obj.covs(1:alpha, 1:alpha, closest_gaussian_id);
            knownGauss_Mean = obj.means(closest_gaussian_id, 1:alpha);
            
            mahaldist = (X-knownGauss_Mean)'*inv(known_Cov)*(X-knownGauss_Mean);
            mu_trend_i = X+(knownGauss_Mean-X)*R/mahaldist;
            
            if isempty(obj.target_means)
                mu_trend_t = mean(obj.means(:, alpha+1:alpha+beta));
            else
                mu_trend_t = obj.target_means;
            end
            
            %trabalhar como se [mu_trend_i,mu_trend_t] fosse um ponto p/
            %ser treinado
%             
            obj.nc = obj.nc + 1;
            obj.means(obj.nc,:) = [mu_trend_i,mu_trend_t];
            obj.sps(1,obj.nc) = 1.0; %sps: prob a posteriori
            obj.vs(1,obj.nc) = 0; %idade do componente [nc]
            rot_matrix = [cos(obj.ang_rot) -sin(obj.ang_rot); sin(obj.ang_rot) cos(obj.ang_rot)];
            cov_m = diag(obj.SIGMA);
%             obj.covs(:,:,obj.nc) = rot_matrix*cov_m*(rot_matrix');
            obj.covs(:,:,obj.nc) = cov_m;
%             obj.priors(obj.nc) = obj.priors(closest_gaussian_id);


            obj = obj.updatePriors();
            
              [obj.loglike(1,obj.nc), obj.mahalaD(1, obj.nc)] = obj.logmvnpdf([mu_trend_i,mu_trend_t], obj.means(obj.nc,:), obj.covs(:,:,obj.nc));
            
            
              obj = obj.computePosterior();
              obj = obj.updateComponents([mu_trend_i,mu_trend_t]);
            
            for j = 1:N
                x = X(j,:);
                
                alpha = size(x, 2);
                beta = size(obj.means, 2) - alpha;
                
                pajs = zeros(obj.nc, 1);
                xm = zeros(obj.nc, beta);
                means_gauss = [];
                dist = zeros(obj.nc);
                for i=1:obj.nc
                    meanA = obj.means(i, 1:alpha);
                    meanB = obj.means(i, alpha+1:alpha+beta);

                    if obj.covType == 2 % full covariance
                        covA = obj.covs(1:alpha, 1:alpha, i);
                        covBA = obj.covs(alpha+1:alpha+beta, 1:alpha, i);
                        covBB = obj.covs(alpha+1:alpha+beta,alpha+1:alpha+beta,i);   
                        covBA_div_covA = covBA / covA;
                        covm(:,:,i) = covBB-covBA_div_covA*covBA';
                        
                        
                        xm(i,:) = meanB + (covBA / covA * (x - meanA)')';
                        
                    else
                        covA = obj.covs(1:alpha); 
                       
                        xm(i,:) = meanB;
                    end
                    [loglike,dist(i),~] = obj.logmvnpdf(x, meanA, covA);
                    pajs(i) = exp(loglike) * obj.priors(i);
                    
                end
                

                pajs = pajs ./ sum(pajs);
                
                
                
                
                
                B(j,:) = sum(bsxfun(@times, xm, pajs));
            end
            
            %calculate variance:
           
                %fprintf('meank\txmk\tBj\tpajs\tcovm\n');
                Cov = zeros(size(obj.means,2) - size(X, 2));
                variogram_range = R;
                for k=1:obj.nc                    
                    contribution_to_variance_k = pajs(k)*(covm(:,:,k)+ sqrt(sum((xm(k,:)-B(j,:)).^2))^2);
                    
                    %Test 14/03/2016. Goal: reduce high frequency by making
                    %the variance proportional to the distance to known
                    %points:
                    
                    factor = (1-exp(-dist(k)/(variogram_range*10))); %exp
                    %factor = (3*dist(k))/(2*variogram_range) - (dist(k)^3)/(2*variogram_range^3);             
                    
                    contribution_to_variance_k = contribution_to_variance_k*factor;
                    
                    %contribution_to_variance_k = 0; 
                   
                    %Original variance                    
                   Cov(:,:,j) = Cov(:,:,j) + contribution_to_variance_k;
                end
                
                if(mahaldist > variogram_range)
                    Cov(:,:,j) = GLOBAL_VARIANCE;
                end
                
                
                
            out = {B, Cov,{[mu_trend_i,mu_trend_t],obj.covs(:, :, end)}};
        end
        % Calculate the indexes assignments for the data points in X for cluster analysis
        function idx = cluster(obj, X)
            N = size(X,1);
            idx = zeros(N,1);
            for i=1:N
                x = X(i,:);
                obj = obj.computeLikelihood(x);
                obj = obj.computePosterior();
                [~,idx(i)] = max(obj.post);
            end
        end
        
        % Compute AIC and BIC Information Criterion
        function obj = computeInfoCriteria(obj)
            dim = size(obj.means,2);
            if obj.covType == 1
                nParam = dim * obj.nc;
            else
                nParam = obj.nc * dim * (dim + 1) / 2;
            end
            NlogL = - obj.dataLikelihood;
            nParam = nParam + obj.nc - 1 + obj.nc * dim;
            obj.BIC = 2 * NlogL + nParam * log(obj.sampleSize);
            obj.AIC = 2 * NlogL + 2 * nParam;
        end
        
    end %end methods
    
end % end class

